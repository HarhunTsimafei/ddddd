//
//  THFilterViewController.h
//  ipsen2
//
//  Created by Timofei Harhun on 08.07.15.
//  Copyright (c) 2015 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface THFilterViewController : UIViewController

- (IBAction)dismissWithResult:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *containerView;


@end
