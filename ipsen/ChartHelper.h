//
//  ChartHelper.h
//  ipsen
//
//  Created by oles on 7/16/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MONEY_NUMBER(param)         [[ChartHelper currencyFormatter] stringFromNumber:[NSNumber numberWithLongLong:param]]
#define GROUPED_NUMBER(param)       [[ChartHelper numberFormatter] stringFromNumber:[NSNumber numberWithLongLong:param]]


@interface ChartHelper : NSObject

+ (NSNumberFormatter *)currencyFormatter;
+ (NSNumberFormatter *)numberFormatter;

@end

