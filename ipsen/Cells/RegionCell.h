//
//  RegionCell.h
//  ipsen
//
//  Created by oles on 8/5/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
