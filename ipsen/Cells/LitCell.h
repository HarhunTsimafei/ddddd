//
//  LitCell.h
//  ipsen
//
//  Created by oles on 8/12/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LitCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *litTitle;

@end
