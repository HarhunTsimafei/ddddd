//
//  SideCell.h
//  ipsen
//
//  Created by oles on 8/12/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *itemTitle;
@property (weak, nonatomic) IBOutlet UILabel *itemFreq;
@property (weak, nonatomic) IBOutlet UILabel *item1Cost;
@property (weak, nonatomic) IBOutlet UILabel *itemResConst;

@end
