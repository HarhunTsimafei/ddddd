//
//  AppDelegate.h
//  ipsen
//
//  Created by oles on 7/15/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


// calc methods

- (void) calcExpensesAnysis;
- (void)recountData;

// all data
@property (nonatomic, strong) NSDictionary *dict;

@property (nonatomic, strong) NSArray *regions;
@property (assign, nonatomic) NSInteger currentRegion;
@property (nonatomic) long long sickInAllRegions;

@property(nonatomic)long long writemh;
@property(assign, nonatomic) BOOL mh;
@property (nonatomic) long long dbmh;
@property (nonatomic) long long nonemh;

// initial data
@property (nonatomic, strong) NSString *regionString;



@property (nonatomic) long long multiplier;

@property (nonatomic) long long priceDisport;
@property (nonatomic) long long priceBotox;
@property (nonatomic) long long priceXeomin;

@property (nonatomic) long long gdpPerHabitant;
@property (nonatomic) long long averageWage1;
@property (nonatomic) long long averageWage2;

@property (nonatomic) long long habitants;
@property (nonatomic) long long child_sick;

@property (nonatomic) long long qtyInsult;
@property (nonatomic) long long qtySick;
@property (nonatomic) long long qtySick3M;
@property (nonatomic) long long qtySick1Y;
@property (nonatomic) long long qtySick5Y;

@property (nonatomic) long long disportSickMonthes;
@property (nonatomic) long long botoxSickMonthes;
@property (nonatomic) long long xeominSickMonthes;
@property (nonatomic) long long noneSickMonthes;

@property (nonatomic) float  ashfortDisport;
@property (nonatomic) float  ashfortBotox;
@property (nonatomic) float  ashfortXeomin;
@property (nonatomic) float  ashfortNone;

@property (nonatomic) float  sickPercent1Group;
@property (nonatomic) float  sickPercent2Group;
@property (nonatomic) float  sickPercent3Group;
@property (nonatomic) float  sickPercentNoneGroup;

@property (nonatomic) long long invPayment1Group;
@property (nonatomic) long long invPayment2Group;
@property (nonatomic) long long invPayment3Group;
@property (nonatomic) long long invPaymentNoneGroup;

@property (nonatomic) long long monthlyPayment1Group;
@property (nonatomic) long long monthlyPayment2Group;
@property (nonatomic) long long monthlyPayment3Group;
@property (nonatomic) long long monthlyPaymentNoneGroup;

// entry data
@property (nonatomic) long populationForCalc;   // 1 or all
@property (nonatomic) long monthsForCalc;

@property (nonatomic) long populationForCalcApp;
@property (nonatomic) CGFloat yearForCalc;
// Calculations
// I. Expences analysis
// 1. GDP loss

@property (nonatomic) long long disportGdpLossTotal;
@property (nonatomic) long long botoxGdpLossTotal;
@property (nonatomic) long long xeominGdpLossTotal;
@property (nonatomic) long long noneGdpLossTotal;

// 2. price of drug per month
@property (nonatomic) long long disportTotalPrice;
@property (nonatomic) long long botoxTotalPrice;
@property (nonatomic) long long xeominTotalPrice;
@property (nonatomic) long long noneTotalPrice;


// 3. payouts (disablement) per month
@property (nonatomic) long long disportDisPayouts;
@property (nonatomic) long long botoxDisPayouts;
@property (nonatomic) long long xeominDisPayouts;
@property (nonatomic) long long noneDisPayouts;

// 4. payouts (inv) per month
@property (nonatomic) long long disportInvPayouts;
@property (nonatomic) long long botoxInvPayouts;
@property (nonatomic) long long xeominInvPayouts;
@property (nonatomic) long long noneInvPayouts;

// 5. side treatment per month
@property (nonatomic) long long disportSideTreatment;
@property (nonatomic) long long botoxSideTreatment;
@property (nonatomic) long long xeominSideTreatment;
@property (nonatomic) long long noneSideTreatment;

// 6. insult treatment per month
@property (nonatomic) long long disportInsultTreatment;
@property (nonatomic) long long botoxInsultTreatment;
@property (nonatomic) long long xeominInsultTreatment;
@property (nonatomic) long long noneInsultTreatment;

// 7. Costs per person
@property (nonatomic) long long disportCostsToxinA;
@property (nonatomic) long long disportCostsMedicalHelp;
@property (nonatomic) long long disportCostsRelaxMedicalHelp;
@property (nonatomic) long long disportCostsPension;
@property (nonatomic) CGFloat disportFourMouthCostsOperations;
@property (nonatomic) CGFloat disportOneYearCostsOperations;
@property (nonatomic) CGFloat disportTwoYearsCostsOperations;
@property (nonatomic) long long disportCostsPlastering;
@property (nonatomic) long long disportCostsCompensation;
@property (nonatomic) long long disportCostsAllowanceForKids;

@property (nonatomic) long long botoxCostsToxinA;
@property (nonatomic) long long botoxCostsMedicalHelp;
@property (nonatomic) long long botoxCostsRelaxMedicalHelp;
@property (nonatomic) long long botoxCostsPension;
@property (nonatomic) CGFloat botoxFourMouthCostsOperations;
@property (nonatomic) CGFloat botoxOneYearCostsOperations;
@property (nonatomic) CGFloat botoxTwoYearsCostsOperations;
@property (nonatomic) long long botoxCostsPlastering;
@property (nonatomic) long long botoxCostsCompensation;
@property (nonatomic) long long botoxCostsAllowanceForKids;

@property (nonatomic) long long noneCostsToxinA;
@property (nonatomic) long long noneCostsMedicalHelp;
@property (nonatomic) long long noneCostsRelaxMedicalHelp;
@property (nonatomic) long long noneCostsPension;
@property (nonatomic) CGFloat noneFourMouthCostsOperations;
@property (nonatomic) CGFloat noneOneYearCostsOperations;
@property (nonatomic) CGFloat noneTwoYearsCostsOperations;
@property (nonatomic) long long noneCostsPlastering;
@property (nonatomic) long long noneCostsCompensation;
@property (nonatomic) long long noneCostsAllowanceForKids;

//ExpenseEffect
@property (nonatomic) CGFloat disportFourMouthExpenseEffect;
@property (nonatomic) CGFloat disportOneYearExpenseEffect;
@property (nonatomic) CGFloat disportTwoYearsExpenseEffect;

@property (nonatomic) CGFloat botoxFourMouthExpenseEffect;
@property (nonatomic) CGFloat botoxOneYearExpenseEffect;
@property (nonatomic) CGFloat botoxTwoYearsExpenseEffect;

@property (nonatomic) CGFloat noneFourMouthExpenseEffect;
@property (nonatomic) CGFloat noneOneYearExpenseEffect;
@property (nonatomic) CGFloat noneTwoYearsExpenseEffect;

@property (nonatomic) CGFloat costsOperations;


@property (nonatomic) CGFloat countCoursesPerYear;

@end
