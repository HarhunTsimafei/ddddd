//
//  Config.h
//  ipsen
//
//  Created by oles on 7/15/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#ifndef ipsen_Config_h
#define ipsen_Config_h

//#define NOTIFICATION_EXP_3M_1C      @"exp3M1C"
//#define NOTIFICATION_EXP_1Y_1C      @"exp1Y1C"
//#define NOTIFICATION_EXP_3M_ALL     @"exp3MALL"
//#define NOTIFICATION_EXP_1Y_ALL     @"exp1YALL"

#define CACHE_KEY                     @"dataCache"
#define DATA_LOADED_KEY               @"dataLoaded"

//#define FILE_URL_PATH_DEF             @"http://www.ipsen.ru/admin/plist/data.plist"
#define FILE_URL_PATH_DEF             @"http://www.ipsen.ru/admin/plist/data2.plist"

#define NOTIFICATION_DATA_CHANGED     @"dataChanged"
#define NOTIFICATION_UPDATE_UI        @"updateUI"


#define INSULT_KOEF                   0.0031322448170066f
#define SICK_NOW                      0.002 // (200*D219)/100000

#define SICK_3M                      (0.8*(app.habitants/500)*0.5)+(0.2*(app.habitants/500)*0.24)+((0.8*(app.qtyInsult/3)*0.8)/4)+((0.2*(app.qtyInsult/3)*0.625)/4)
#define SICK_3M_APP                  (0.8*(self.habitants/500)*0.5)+(0.2*(self.habitants/500)*0.24)+((0.8*(self.qtyInsult/3)*0.8)/4)+((0.2*(self.qtyInsult/3)*0.625)/4)

#define SICK_1Y                      (0.8*(app.habitants/500)*0.5)+(0.2*(app.habitants/500)*0.24)+(0.8*(app.qtyInsult/3)*0.6)+(0.2*(app.qtyInsult/3)*0.38)
#define SICK_1Y_APP                  (0.8*(self.habitants/500)*0.5)+(0.2*(self.habitants/500)*0.24)+(0.8*(self.qtyInsult/3)*0.6)+(0.2*(self.qtyInsult/3)*0.38)

// ((0.8*(L6/500)*0.25))+((0.2*(L6/500)*0.18)+((0.8*(N10/3)*0.5*5))+((0.2*(N10/3)*0.24*5)))

#define SICK_5Y                      ((0.8*(app.habitants/500)*0.25))+((0.2*(app.habitants/500)*0.18)+((0.8*(app.qtyInsult/3)*0.5*5))+((0.2*(app.qtyInsult/3)*0.24*5)))
#define SICK_5Y_APP                  ((0.8*(self.habitants/500)*0.25))+((0.2*(self.habitants/500)*0.18)+((0.8*(self.qtyInsult/3)*0.5*5))+((0.2*(self.qtyInsult/3)*0.24*5)))

#define SICK_5Y_FIXED                (app.habitants/100)+(app.habitants/18)+((app.qtyInsult*2)/3)+(app.qtyInsult/0.76)
#define SICK_5Y_APP_FIXED            (self.habitants/100)+(self.habitants/18)+((self.qtyInsult*2)/3)+(self.qtyInsult/0.76))

#define SICK_1Y_FIXED                (app.habitants/200)+(app.habitants/240)+(app.qtyInsult*0.16)+((app.qtyInsult/3)*0.076)
#define SICK_1Y_APP_FIXED            (self.habitants/200)+(self.habitants/240)+(self.qtyInsult*0.16)+((self.qtyInsult/3)*0.076)



#define COLOR_PREP                         [UIColor colorWithRed:119.0f/255.0f green:0.0f/255.0f blue:119.0f/255.0f alpha:1.0f]
#define COLOR_DIS                         [UIColor colorWithRed:140.0f/255.0f green:162.0f/255.0f blue:91.0f/255.0f alpha:1.0f]
#define COLOR_GDP                         [UIColor colorWithRed:191.0f/255.0f green:82.0f/255.0f blue:77.0f/255.0f alpha:1.0f]
#define COLOR_INS                         [UIColor colorWithRed:224.0f/255.0f green:193.0f/255.0f blue:114.0f/255.0f alpha:1.0f]
#define COLOR_SIDE                         [UIColor colorWithRed:194.0f/255.0f green:223.0f/255.0f blue:229.0f/255.0f alpha:1.0f]
#define COLOR_INV                         [UIColor colorWithRed:123.0f/255.0f green:188.0f/255.0f blue:204.0f/255.0f alpha:1.0f]

#define COLOR_CostsToxinA              [UIColor colorWithRed:106.0f/255.0f green:0.0f/255.0f blue:111.0f/255.0f alpha:1.0f]
#define COLOR_CostsMedicalHelp         [UIColor colorWithRed:213.0f/255.0f green:88.0f/255.0f blue:21.0f/255.0f alpha:1.0f]
#define COLOR_CostsRelaxMedicalHelp    [UIColor colorWithRed:134.0f/255.0f green:157.0f/255.0f blue:83.0f/255.0f alpha:1.0f]
#define COLOR_CostsPension             [UIColor colorWithRed:179.0f/255.0f green:74.0f/255.0f blue:69.0f/255.0f alpha:1.0f]
#define COLOR_CostsOperations          [UIColor colorWithRed:75.0f/255.0f green:100.0f/255.0f blue:193.0f/255.0f alpha:1.0f]
#define COLOR_CostsPlastering          [UIColor colorWithRed:122.0f/255.0f green:183.0f/255.0f blue:200.0f/255.0f alpha:1.0f]
#define COLOR_CostsCompensation        [UIColor colorWithRed:192.0f/255.0f green:220.0f/255.0f blue:227.0f/255.0f alpha:1.0f]
#define COLOR_CostsAllowanceForKids    [UIColor colorWithRed:218.0f/255.0f green:190.0f/255.0f blue:106.0f/255.0f alpha:1.0f]

#define BLUE_COLOR                     [UIColor colorWithRed:59.0f/255.0f green:84.0f/255.0f blue:163.0f/255.0f alpha:1.0f]
#define GREEN_COLOR                     [UIColor colorWithRed:120.0f/255.0f green:165.0f/255.0f blue:46.0f/255.0f alpha:1.0f]
#define RED_COLOR                       [UIColor colorWithRed:153.0f/255.0f green:43.0f/255.0f blue:46.0f/255.0f alpha:1.0f]


#define XEOMIN_COLOR                      [UIColor colorWithRed:160.0f/255.0f green:102.0f/255.0f blue:20.0f/255.0f alpha:1.0f]
#define DISPORT_COLOR                   [UIColor colorWithRed:44.0f/255.0f green:178.0f/255.0f blue:0.0f/255.0f alpha:1.0f]
#define BOTOX_COLOR                     [UIColor colorWithRed:70.0f/255.0f green:0.0f/255.0f blue:140.0f/255.0f alpha:1.0f]
#define NONE_COLOR                    [UIColor colorWithRed:0.0f/255.0f green:89.0f/255.0f blue:178.0f/255.0f alpha:1.0f]


#define BOTOX_VS_DISPORT_COLOR          [UIColor colorWithRed:119.0f/255.0f green:0.0f/255.0f blue:119.0f/255.0f alpha:1.0f]
#define XEOMIN_VS_DISPORT_COLOR          [UIColor colorWithRed:191.0f/255.0f green:82.0f/255.0f blue:77.0f/255.0f alpha:1.0f]
#define NONE_VS_DISPORT_COLOR          [UIColor colorWithRed:123.0f/255.0f green:188.0f/255.0f blue:204.0f/255.0f alpha:1.0f]


#endif
