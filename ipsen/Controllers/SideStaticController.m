//
//  SideStaticController.m
//  ipsen
//
//  Created by oles on 7/17/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "SideStaticController.h"
#import "SideCell.h"

@interface SideStaticController ()

@property (nonatomic, strong) NSDictionary *disportModel;
@property (nonatomic, strong) NSDictionary *botoxModel;
@property (nonatomic, strong) NSDictionary *baklofenModel;

@end

@implementation SideStaticController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"];
//    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:filePath];
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;

    NSDictionary *models = [app.dict objectForKey:@"side"];
    self.disportModel = [models objectForKey:@"disport"];
    self.botoxModel = [models objectForKey:@"botox"];
    self.baklofenModel = [models objectForKey:@"baklofen"];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSDictionary *model;
    switch (self.selectedDrug) {
        case 0:
            model = self.disportModel;
            break;
        case 1:
            model = self.botoxModel;
            break;
        case 2:
            model = self.baklofenModel;
            break;
            
        default:
            break;
    }
    
    NSArray *items = [model objectForKey:@"effects"];
    return items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SideCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SideCell" forIndexPath:indexPath];
    
    // Configure the cell...
    NSDictionary *model;
    switch (self.selectedDrug) {
        case 0:
            model = self.disportModel;
            break;
        case 1:
            model = self.botoxModel;
            break;
        case 2:
            model = self.baklofenModel;
            break;
            
        default:
            break;
    }

    NSDictionary *item = [model objectForKey:@"effects"][indexPath.row];
    cell.itemTitle.text = [item objectForKey:@"name"];
    cell.itemFreq.text = [NSString stringWithFormat:@"%@ %%", [item objectForKey:@"freq"]];
    NSNumber *c1 = [item objectForKey:@"cost1"];
    NSNumber *cRes = [item objectForKey:@"res_cost"];
    cell.item1Cost.text = MONEY_NUMBER(c1.longLongValue);
    cell.itemResConst.text = MONEY_NUMBER(cRes.longLongValue);    
    
    return cell;
}

@end
