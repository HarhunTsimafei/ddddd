//
//  AZChartController.m
//  ipsen
//
//  Created by oles on 7/16/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "AZChartController.h"
#import "ChartHelper.h"

@interface AZChartController ()<CPTPlotDataSource, CPTPlotSpaceDelegate>

@end

@implementation AZChartController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUI) name:NOTIFICATION_UPDATE_UI object:nil];
    [self updateUI];
    //self.chartView.backgroundColor = [UIColor clearColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [self updateUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateUI {
    [self generateData];
    [self generateLayout];
    self.chartView.hostedGraph = graph;

}

- (void)generateLayout
{
    //Create graph from theme
	graph                               = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
	//[graph applyTheme:[CPTTheme themeNamed:kCPTPlainWhiteTheme]];
	
    graph.fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1.0f]];
    //self.hostedGraph                    = graph;
    graph.plotAreaFrame.masksToBorder   = NO;
    graph.paddingLeft                   = 0.0f;
    graph.paddingTop                    = 0.0f;
	graph.paddingRight                  = 0.0f;
	graph.paddingBottom                 = 0.0f;
    
    CPTMutableLineStyle *borderLineStyle    = [CPTMutableLineStyle lineStyle];
	borderLineStyle.lineColor               = [CPTColor whiteColor];
	borderLineStyle.lineWidth               = 0.0f;
	graph.plotAreaFrame.borderLineStyle     = borderLineStyle;
	graph.plotAreaFrame.paddingTop          = 10.0;
	graph.plotAreaFrame.paddingRight        = 10.0;
	graph.plotAreaFrame.paddingBottom       = 80.0;
	graph.plotAreaFrame.paddingLeft         = 70.0;
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.yearForCalc = app.yearForCalc;
    self.populationForCalc = app.populationForCalcApp;
    
    CGFloat year_popul = self.yearForCalc*self.populationForCalc;
    long long calc_max = (app.noneCostsToxinA*year_popul+
                          app.noneCostsMedicalHelp*year_popul+
                          app.noneCostsRelaxMedicalHelp*year_popul+
                          app.noneCostsPension*year_popul+
                          app.noneTwoYearsCostsOperations*year_popul+
                          app.noneCostsPlastering*year_popul+
                          ((self.switchOn)? app.noneCostsCompensation*year_popul:0)
                          +app.noneCostsAllowanceForKids*year_popul)*1.3f;
    
    
	//Add plot space
	CPTXYPlotSpace *plotSpace       = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    plotSpace.delegate              = self;
	plotSpace.yRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)
                                                                   length:CPTDecimalFromLongLong(calc_max)];
	plotSpace.xRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(-1)
                                                                   length:CPTDecimalFromInt(6)];

    //Grid line styles
	CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
	majorGridLineStyle.lineWidth            = 0.75;
	majorGridLineStyle.lineColor            = [[CPTColor blackColor] colorWithAlphaComponent:0.1];
    CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
	minorGridLineStyle.lineWidth            = 0.0;
	minorGridLineStyle.lineColor            = [[CPTColor blackColor] colorWithAlphaComponent:0.1];
    
    //Axes
	CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    
    //X axis
    CPTXYAxis *x                    = axisSet.xAxis;
    x.orthogonalCoordinateDecimal   = CPTDecimalFromInt(0);
	x.majorIntervalLength           = CPTDecimalFromInt(1);
	x.minorTicksPerInterval         = 0;
    x.labelingPolicy                = CPTAxisLabelingPolicyNone;
    x.majorGridLineStyle            = majorGridLineStyle;
    x.axisConstraints               = [CPTConstraints constraintWithLowerOffset:0.0];
    
    //X labels
    int labelLocations = 0;
    NSMutableArray *customXLabels = [NSMutableArray array];
    for (NSString *day in preparats) {
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:day textStyle:x.labelTextStyle];
        newLabel.tickLocation   = [[NSNumber numberWithInt:labelLocations] decimalValue];
        newLabel.offset         = x.labelOffset + x.majorTickLength;
        //newLabel.rotation       = M_PI / 4;
        [customXLabels addObject:newLabel];
        labelLocations++;
    }
    //x.axisLabels                    = [NSSet setWithArray:customXLabels];
    
    //Y axis
	CPTXYAxis *y            = axisSet.yAxis;
	//y.title                 = @"Value";
	y.titleOffset           = 70.0f;
    
    //y.labelOffset           = -10.0f;
    y.labelingPolicy        = CPTAxisLabelingPolicyAutomatic;
    y.majorGridLineStyle    = majorGridLineStyle;
    y.minorGridLineStyle    = minorGridLineStyle;
    y.axisLineStyle         = minorGridLineStyle;
    y.axisConstraints       = [CPTConstraints constraintWithLowerOffset:0.0];
    
    CPTMutableTextStyle *style = [CPTMutableTextStyle textStyle];
    style.fontSize = 10.0f;
    y.labelTextStyle = style;
    
    //Create a bar line style
    CPTMutableLineStyle *barLineStyle   = [[CPTMutableLineStyle alloc] init];
    barLineStyle.lineWidth              = 0.0;
    barLineStyle.lineColor              = [CPTColor whiteColor];
    CPTMutableTextStyle *whiteTextStyle = [CPTMutableTextStyle textStyle];
	whiteTextStyle.color                = [CPTColor whiteColor];
    CPTMutableTextStyle *blackTextStyle = [CPTMutableTextStyle textStyle];
	whiteTextStyle.color                = [CPTColor blackColor];
    
    //Plot
    BOOL firstPlot = YES;
    for (NSString *set in [[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
    //for (NSString *set in [sets allKeys]) {
        CPTBarPlot *plot        = [CPTBarPlot tubularBarPlotWithColor:[CPTColor blueColor] horizontalBars:NO];
        plot.lineStyle          = barLineStyle;
        plot.showLabels         = YES;
        plot.labelTextStyle     = style;
        plot.labelFormatter     = [ChartHelper currencyFormatter];

        CGColorRef color        = ((UIColor *)[sets objectForKey:set]).CGColor;
        plot.fill               = [CPTFill fillWithColor:[CPTColor colorWithCGColor:color]];
        if (firstPlot) {
            plot.barBasesVary   = NO;
            firstPlot           = NO;
        } else {
            plot.barBasesVary   = YES;
        }
        plot.barWidth           = CPTDecimalFromFloat(0.8f);
        plot.barsAreHorizontal  = NO;
        plot.dataSource         = self;
        plot.identifier         = set;
        plot.showLabels = YES;
        [graph addPlot:plot toPlotSpace:plotSpace];
    }
    
    /*
    //Add legend
	CPTLegend *theLegend      = [CPTLegend legendWithGraph:graph];
	theLegend.numberOfRows	  = sets.count;
	theLegend.fill			  = [CPTFill fillWithColor:[CPTColor colorWithCGColor:[UIColor clearColor].CGColor]];
	theLegend.borderLineStyle = barLineStyle;
	theLegend.cornerRadius	  = 0.0;
	theLegend.swatchSize	  = CGSizeMake(18.0, 18.0);
	whiteTextStyle.fontSize	  = 10.0;
	theLegend.textStyle		  = blackTextStyle;
	theLegend.rowMargin		  = 5.0;
	theLegend.paddingLeft	  = 10.0;
	theLegend.paddingTop	  = 10.0;
	theLegend.paddingRight	  = 10.0;
	theLegend.paddingBottom	  = 10.0;
	graph.legend              = theLegend;
    graph.legendAnchor        = CPTRectAnchorTopRight;
    graph.legendDisplacement  = CGPointMake(80.0, -10.0);
     */
}

- (void)generateData
{
    NSMutableDictionary *dataTemp = [[NSMutableDictionary alloc] init];
    
    
    //Array containing all the dates that will be displayed on the X axis
    preparats = [NSArray arrayWithObjects:@"Диспорт", @"Ботокс",
             @"Стандартная терапия без БТА", nil];
    
    //Dictionary containing the name of the two sets and their associated color
    //used for the demo

    sets = [NSDictionary dictionaryWithObjectsAndKeys:
            COLOR_CostsToxinA, @"8",
            COLOR_CostsMedicalHelp, @"7",
            COLOR_CostsRelaxMedicalHelp, @"6",
            COLOR_CostsPension, @"5",
            COLOR_CostsOperations, @"4",
            COLOR_CostsPlastering, @"3",
            COLOR_CostsCompensation, @"2",
            COLOR_CostsAllowanceForKids, @"1",
            nil];
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
  
    self.yearForCalc = app.yearForCalc;
    self.populationForCalc = app.populationForCalcApp;
    
    CGFloat year_popul = self.yearForCalc*self.populationForCalc;
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSNumber numberWithLongLong:app.disportCostsToxinA*year_popul] forKey:@"8"];
    [dict setObject:[NSNumber numberWithLongLong:app.disportCostsMedicalHelp*year_popul] forKey:@"7"];
    [dict setObject:[NSNumber numberWithLongLong:app.disportCostsRelaxMedicalHelp*year_popul] forKey:@"6"];
    [dict setObject:[NSNumber numberWithLongLong:app.disportCostsPension*year_popul] forKey:@"5"];
    CGFloat costsPlast = app.disportCostsPlastering*year_popul;
    switch (app.monthsForCalc) {
        case 4:
            [dict setObject:[NSNumber numberWithLongLong:app.disportFourMouthCostsOperations*year_popul] forKey:@"4"];
            costsPlast = 25637* self.populationForCalc;
            break;
        case 12:
            [dict setObject:[NSNumber numberWithLongLong:app.disportOneYearCostsOperations*year_popul] forKey:@"4"];
    
            break;
        case 24:
            [dict setObject:[NSNumber numberWithLongLong:app.disportTwoYearsCostsOperations*year_popul] forKey:@"4"];
           
            break;
            
        default:
            break;
    }
    
    [dict setObject:[NSNumber numberWithLongLong:costsPlast] forKey:@"3"];
    (self.switchOn)?[dict setObject:[NSNumber numberWithLongLong:app.disportCostsCompensation*year_popul] forKey:@"2"]:NSLog(@"1") ;
    [dict setObject:[NSNumber numberWithLongLong:app.disportCostsAllowanceForKids*year_popul] forKey:@"1"];
    [dataTemp setObject:dict forKey:preparats[0]];
    
    dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSNumber numberWithLongLong:app.botoxCostsToxinA*year_popul] forKey:@"8"];
    [dict setObject:[NSNumber numberWithLongLong:app.botoxCostsMedicalHelp*year_popul] forKey:@"7"];
    [dict setObject:[NSNumber numberWithLongLong:app.botoxCostsRelaxMedicalHelp*year_popul] forKey:@"6"];
    [dict setObject:[NSNumber numberWithLongLong:app.botoxCostsPension*year_popul] forKey:@"5"];
    costsPlast = app.botoxCostsPlastering*year_popul;
    switch (app.monthsForCalc) {
        case 4:
            costsPlast = 25637* self.populationForCalc;
            [dict setObject:[NSNumber numberWithLongLong:app.botoxFourMouthCostsOperations*year_popul] forKey:@"4"];
            
            break;
        case 12:
            [dict setObject:[NSNumber numberWithLongLong:app.botoxOneYearCostsOperations*year_popul] forKey:@"4"];
            
            break;
        case 24:
            [dict setObject:[NSNumber numberWithLongLong:app.botoxTwoYearsCostsOperations*year_popul] forKey:@"4"];
            
            break;
            
        default:
            break;
    }
    [dict setObject:[NSNumber numberWithLongLong:costsPlast] forKey:@"3"];
    (self.switchOn)?[dict setObject:[NSNumber numberWithLongLong:app.botoxCostsCompensation*year_popul] forKey:@"2"]:NSLog(@"1") ;
    [dict setObject:[NSNumber numberWithLongLong:app.botoxCostsAllowanceForKids*year_popul] forKey:@"1"];
    [dataTemp setObject:dict forKey:preparats[1]];
    
    dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSNumber numberWithLongLong:app.noneCostsToxinA*year_popul] forKey:@"8"];
    [dict setObject:[NSNumber numberWithLongLong:app.noneCostsMedicalHelp*year_popul] forKey:@"7"];
    [dict setObject:[NSNumber numberWithLongLong:app.noneCostsRelaxMedicalHelp*year_popul] forKey:@"6"];
    [dict setObject:[NSNumber numberWithLongLong:app.noneCostsPension*year_popul] forKey:@"5"];
    costsPlast = app.noneCostsPlastering*year_popul;
    switch (app.monthsForCalc) {
        case 4:
            [dict setObject:[NSNumber numberWithLongLong:app.noneFourMouthCostsOperations*year_popul] forKey:@"4"];
            costsPlast = 25637* self.populationForCalc;
            break;
        case 12:
            [dict setObject:[NSNumber numberWithLongLong:app.noneOneYearCostsOperations*year_popul] forKey:@"4"];
            
            break;
        case 24:
            [dict setObject:[NSNumber numberWithLongLong:app.noneTwoYearsCostsOperations*year_popul] forKey:@"4"];
            
            break;
            
        default:
            break;
    }
    [dict setObject:[NSNumber numberWithLongLong:costsPlast] forKey:@"3"];
    (self.switchOn)?[dict setObject:[NSNumber numberWithLongLong:app.noneCostsCompensation*year_popul] forKey:@"2"]:NSLog(@"1");
    [dict setObject:[NSNumber numberWithLongLong:app.noneCostsAllowanceForKids*year_popul] forKey:@"1"];
    [dataTemp setObject:dict forKey:preparats[2]];

    
    data = [dataTemp copy];
    
    NSLog(@"%@", data);
}

#pragma mark - CPTPlotDataSource methods

- (NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    return preparats.count;
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)idx {
    
    NSNumber *num = [NSNumber numberWithFloat:NAN];
    
    if (fieldEnum == 0) {
        num = [NSNumber numberWithFloat:idx];
    }
    
    else {
        CGFloat offset = 0;
        if (((CPTBarPlot *)plot).barBasesVary) {
            for (NSString *set in [[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
                //for (NSString *set in [sets allKeys]) {
                if ([plot.identifier isEqual:set]) {
                    break;
                }
                offset += [[[data objectForKey:[preparats objectAtIndex:idx]] objectForKey:set] floatValue];
            }
        }
        
        //Y Value
        if (fieldEnum == 1) {
            num = [NSNumber numberWithFloat:[[[data objectForKey:[preparats objectAtIndex:idx]] objectForKey:plot.identifier] floatValue] + offset];
        }
        
        //Offset for stacked bar
        else {
            num = [NSNumber numberWithFloat:offset];
        }
    }
    
    return num;

}

//-(CPTLayer *)dataLabelForPlot:(CPTPlot *)plot recordIndex:(NSUInteger)idx {
//    NSString *pl_id = (NSString *)plot.identifier;
//    if ([pl_id isEqualToString:@"5.Затраты на препараты Ботулинического токсина типа А"])
//        return nil;
//    else
//        return [NSNull null];
//}

/*
- (double)doubleForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    long long num = NAN;
    
    //X Value
    if (fieldEnum == 0) {
        num = index;
    }
    
    else {
        long long offset = 0;
        if (((CPTBarPlot *)plot).barBasesVary) {
//            for (NSString *set in [[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
            for (NSString *set in [sets allKeys]) {
                if ([plot.identifier isEqual:set]) {
                    break;
                }
                offset += [[[data objectForKey:[preparats objectAtIndex:index]] objectForKey:set] longLongValue];
            }
        }
        
        //Y Value
        if (fieldEnum == 1) {
            num = [[[data objectForKey:[preparats objectAtIndex:index]] objectForKey:plot.identifier] longLongValue] + offset;
        }
        
        //Offset for stacked bar
        else {
            num = offset;
        }
    }
    
    //NSLog(@"%@ - %d - %d - %f", plot.identifier, index, fieldEnum, num);
    
    return num;
}
*/


@end
