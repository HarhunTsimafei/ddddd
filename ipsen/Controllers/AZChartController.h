//
//  AZChartController.h
//  ipsen
//
//  Created by oles on 7/16/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CorePlot-CocoaTouch.h>

@interface AZChartController : UIViewController
{
    CPTXYGraph *graph;
    
    NSDictionary *data;
    NSDictionary *sets;
    NSArray *preparats;
}

@property (assign, nonatomic) BOOL switchOn;
- (void)updateUI ;

@property (nonatomic) long populationForCalc;
@property (nonatomic) CGFloat yearForCalc;

@property (weak, nonatomic) IBOutlet CPTGraphHostingView *chartView;

@property (weak, nonatomic) IBOutlet UILabel *disportTotal;
@property (weak, nonatomic) IBOutlet UILabel *botoxTotal;
@property (weak, nonatomic) IBOutlet UILabel *xeominTotal;
@property (weak, nonatomic) IBOutlet UILabel *noneTotal;

@end
