//
//  ASController.m
//  ipsen
//
//  Created by oles on 7/18/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "ASController.h"

@interface ASController ()

@end

@implementation ASController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self refreshData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)disportChanged:(id)sender {
    [self refreshData];
}

- (IBAction)botoxChanged:(id)sender {
    [self refreshData];
}

- (IBAction)xeominChanged:(id)sender {
    [self refreshData];
}

- (void)refreshData {
    float disportChange = self.disportSlider.value;
    float botoxChange = self.botoxSlider.value;
    float xeominChange = self.xeominSlider.value;
    
    self.disportValue.text = [NSString stringWithFormat:@"%.0f %%", disportChange*100];
    self.botoxValue.text = [NSString stringWithFormat:@"%.0f %%", botoxChange*100];
    self.xeominValue.text = [NSString stringWithFormat:@"%.0f %%", xeominChange*100];
    
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    long long disport_total_1_1Y =
    ((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*(1-0.17)))
    +
    ((app.priceDisport*2)*(12/3))
    +
    (((app.priceDisport*2)*(12/3))*disportChange)
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*5*(1-0.17))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*12*(1-0.17f))
    +
    (16538/5/((1)?1:4)*(1))
    +
    ((177793+32287)*(1));
    
    
    
    long long botox_total_1_1Y =
    ((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*(1-0.127)))
    +
    ((app.priceBotox*3)*(12/3))
    +
    (((app.priceBotox*3)*(12/3))*botoxChange)
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*5*(1-0.127))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*12*(1-0.127f))
    +
    (22250/5/((1)?1:4)*((1)?1:(1?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((1)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    
    
    long long xeomin_total_1_1Y =
    ((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*(1-0.1233)))
    +
    ((app.priceXeomin*4)*(12/3))
    +
    (((app.priceXeomin*4)*(12/3))*xeominChange)
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*5*(1-0.1233))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*12*(1-0.1233f))
    +
    (24029/5/((1)?1:4)*((1)?1:(1?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((1)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    
    long long none_total_1_1Y =
    ((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*(1-0.0333)))
    +
    0
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*5*(1-0.0333))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*12*(1-0.0333f))
    +
    0
    +
    ((177793+32287)*((1)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    self.disportLabel.text = MONEY_NUMBER(disport_total_1_1Y/app.ashfortDisport);
    self.botoxLabel.text = MONEY_NUMBER(botox_total_1_1Y/app.ashfortBotox);
    self.xeominLabel.text = MONEY_NUMBER(xeomin_total_1_1Y/app.ashfortXeomin);
    self.noneLabel.text = MONEY_NUMBER(none_total_1_1Y/app.ashfortNone);
    
}

@end
