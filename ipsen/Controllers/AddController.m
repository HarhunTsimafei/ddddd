//
//  AddController.m
//  ipsen
//
//  Created by oles on 8/4/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "AddController.h"
#import "StandartsController.h"
#import "SideController.h"
#import "SourcesController.h"

@interface AddController ()

@property (nonatomic) int selectedItem;

@property (strong, nonatomic) UIPageViewController *pageController;
@property (strong, nonatomic) SideController *sideController;
@property (strong, nonatomic) SourcesController *sourcesController;

@end

@implementation AddController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.standartsButton.selected = YES;
    [self setupPageController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

///*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@""]) {
        StandartsController *controller = (StandartsController *)segue.destinationViewController;
        controller.selectedItem = self.selectedItem;
    }
}
//*/


#pragma marks - Controllers

- (SideController *)sideController {
    if (!_sideController) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _sideController = (SideController *)[storyBoard instantiateViewControllerWithIdentifier:@"Side"];
    }
    return _sideController;
}

- (SourcesController *)sourcesController {
    if (!_sourcesController) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _sourcesController = (SourcesController *)[storyBoard instantiateViewControllerWithIdentifier:@"Sources"];
    }
    return _sourcesController;
}

- (void)setupPageController {
    NSArray *viewControllers = @[self.sideController];
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.pageController.delegate = self;
    self.pageController.dataSource = self;
    
    // disable swipe
    for (UIScrollView *view in self.pageController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            view.scrollEnabled = NO;
        }
    }
    
    [self addChildViewController:self.pageController];
    [self.containerView addSubview:self.pageController.view];
    
    // Set the page view controller's bounds using an inset rect so that self's view is visible around the edges of the pages.
    CGRect pageViewRect = self.containerView.bounds;
    pageViewRect = CGRectMake(pageViewRect.origin.x, pageViewRect.origin.y, pageViewRect.size.width, pageViewRect.size.height);
    self.pageController.view.frame = pageViewRect;
    
    [self.pageController didMoveToParentViewController:self];
    
    // Add the page view controller's gesture recognizers to the book view controller's view so that the gestures are started more easily.
    self.view.gestureRecognizers = self.pageController.gestureRecognizers;
}

- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    UIViewController *currentViewController = self.pageController.viewControllers[0];
    NSArray *viewControllers = @[currentViewController];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    
    self.pageController.doubleSided = NO;
    return UIPageViewControllerSpineLocationMin;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    return self.sideController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    return self.sideController;
}

- (IBAction)selectStandarts:(id)sender {
    [self performSegueWithIdentifier:@"ShowStandarts" sender:self];
}

- (IBAction)selectSide:(id)sender {
    self.standartsButton.selected = NO;
    self.sideButton.selected = YES;
    self.litButton.selected = NO;
    self.titleView.hidden = YES;
    
    NSArray *array = @[self.sideController];
    [self.pageController setViewControllers:array direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];

}

- (IBAction)selectLit:(id)sender {
    self.standartsButton.selected = NO;
    self.sideButton.selected = NO;
    self.litButton.selected = YES;
    self.titleView.hidden = YES;

    NSArray *array = @[self.sourcesController];
    [self.pageController setViewControllers:array direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
}

- (IBAction)selectInsult:(id)sender {
    self.selectedItem = 1;
    [self performSegueWithIdentifier:@"ShowStandarts" sender:self];
}

- (IBAction)selectInsultAfter:(id)sender {
    self.selectedItem = 2;
    [self performSegueWithIdentifier:@"ShowStandarts" sender:self];
}

- (IBAction)selectDerm:(id)sender {
    self.selectedItem = 3;
    [self performSegueWithIdentifier:@"ShowStandarts" sender:self];
}

- (IBAction)selectBurs:(id)sender {
    self.selectedItem = 4;
    [self performSegueWithIdentifier:@"ShowStandarts" sender:self];
}

- (IBAction)selectDepr:(id)sender {
    self.selectedItem = 5;
    [self performSegueWithIdentifier:@"ShowStandarts" sender:self];
}

- (IBAction)selectHead:(id)sender {
    self.selectedItem = 6;
    [self performSegueWithIdentifier:@"ShowStandarts" sender:self];
}

@end
