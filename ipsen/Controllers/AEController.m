//
//  AEController.m
//  ipsen
//
//  Created by oles on 7/17/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "AEController.h"

@interface AEController ()

@end

@implementation AEController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tabChanged:(id)sender {
    if (self.tabSwitcher.selectedSegmentIndex == 0) {
        self.titleLabel.text = @"Результаты сравнения снижения инвалидизации при терапии БТА на 4й неделе после инъекции по шкале Disability Assessment Scale";
        self.view1.hidden = NO;
        self.view2.hidden = YES;
        self.view3.hidden = YES;
    } else if (self.tabSwitcher.selectedSegmentIndex == 1){
        self.titleLabel.text = @"Изменение в % по шкале Больших моторных функций (раздел Ходьба) (GMFM-88)";
        self.view1.hidden = YES;
        self.view2.hidden = NO;
        self.view3.hidden = YES;
    } else {
        self.titleLabel.text = @"Успешность терапии (% пациентов, избежавших хирургических вмешательств)";
        self.view1.hidden = YES;
        self.view2.hidden = YES;
        self.view3.hidden = NO;
    }
}

@end
