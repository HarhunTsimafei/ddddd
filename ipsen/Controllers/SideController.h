//
//  SideController.h
//  ipsen
//
//  Created by oles on 7/17/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *tabSwitcher;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *headerView2;
@property (weak, nonatomic) IBOutlet UILabel *sideCost;

- (IBAction)changeTab:(id)sender;

@end
