//
//  BCController.m
//  ipsen
//
//  Created by oles on 8/8/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "BCController.h"

@interface BCController ()<CPTPieChartDataSource>

@property (nonatomic) long long disport_total_ALL_1Y;
@property (nonatomic) long long botox_total_ALL_1Y;
@property (nonatomic) long long xeomin_total_ALL_1Y;
@property (nonatomic) long long none_total_ALL_1Y;

@property (nonatomic) long long curValue;
@property (nonatomic) long long modValue;

@end

@implementation BCController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:NOTIFICATION_UPDATE_UI object:nil];

    sets = [NSDictionary dictionaryWithObjectsAndKeys:
            DISPORT_COLOR, @"0",
            BOTOX_COLOR, @"1",
            XEOMIN_COLOR, @"3",
            NONE_COLOR, @"2",
            nil];

    [self refreshData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)preparePie1 {
    graph1 = [[CPTXYGraph alloc] initWithFrame: self.view.bounds];
	self.chartView1.hostedGraph = graph1;
    
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph1.axisSet;
    CPTXYAxis *x                    = axisSet.xAxis;
    CPTXYAxis *y                    = axisSet.yAxis;
    x.axisLineStyle = nil;
    y.axisLineStyle = nil;

	CPTPieChart *pieChart = [[CPTPieChart alloc] init];
	pieChart.dataSource = self;
	pieChart.pieRadius = 100.0;
	pieChart.pieInnerRadius = 40.0;
	pieChart.identifier = @"cur";
	pieChart.startAngle = M_PI_4;
	pieChart.sliceDirection = CPTPieDirectionCounterClockwise;
    
	pieData1 = [NSMutableArray arrayWithObjects:
                [NSNumber numberWithDouble:self.curDisportSlider.value],
                [NSNumber numberWithDouble:self.curBotoxSlider.value],
                [NSNumber numberWithDouble:self.curXeominSlider.value],
                [NSNumber numberWithDouble:self.curNoneSlider.value],
                nil];
    
    [graph1 removePlotWithIdentifier:@"cur"];
	[graph1 addPlot:pieChart];
}

- (void)preparePie2 {
    graph2 = [[CPTXYGraph alloc] initWithFrame: self.view.bounds];
	self.chartView2.hostedGraph = graph2;

   	CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph2.axisSet;
    CPTXYAxis *x                    = axisSet.xAxis;
    CPTXYAxis *y                    = axisSet.yAxis;
    x.axisLineStyle = nil;
    y.axisLineStyle = nil;
    
	CPTPieChart *pieChart = [[CPTPieChart alloc] init];
	pieChart.dataSource = self;
	pieChart.pieRadius = 100.0;
	pieChart.pieInnerRadius = 40.0;
	pieChart.identifier = @"mod";
	pieChart.startAngle = M_PI_4;
	pieChart.sliceDirection = CPTPieDirectionCounterClockwise;
    
	pieData2 = [NSMutableArray arrayWithObjects:
                [NSNumber numberWithDouble:self.modDisportSlider.value],
                [NSNumber numberWithDouble:self.modBotoxSlider.value],
                [NSNumber numberWithDouble:self.modXeominSlider.value],
                [NSNumber numberWithDouble:self.modNoneSlider.value],
                nil];
    
    [graph2 removePlotWithIdentifier:@"mod"];
	[graph2 addPlot:pieChart];
}

- (void)refreshData {
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.disport_total_ALL_1Y =
//    +
//    ((((app.qtySick1Y*(app.sickPercent1Group/100))+(app.qtySick1Y*(app.sickPercent2Group/100)))*app.gdpPerHabitant*(1-0.17)))
    +
    ((app.priceDisport*2)*(12/3)*(app.qtySick1Y))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*25*(1-0.17)*app.qtySick1Y)
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick1Y*12*(1-0.17f))
    +
    (16538/5/((1)?1:4)*((0)?1:(app.qtySick1Y)))
    +
    ((177793+32287)*(((app.qtySick1Y))));

    self.botox_total_ALL_1Y =
//    +
//    ((((app.qtySick1Y*(app.sickPercent1Group/100))+(app.qtySick1Y*(app.sickPercent2Group/100)))*app.gdpPerHabitant*(1-0.127)))
    +
    ((app.priceBotox*3)*(12/3)*(app.qtySick1Y))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*25*(1-0.127)*app.qtySick1Y)
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick1Y*12*(1-0.127f))
    +
    (22250/5/((1)?1:4)*((0)?1:(1?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((0)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    
    
    self.xeomin_total_ALL_1Y =
//    ((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*(1-0.1233)))
//    +
//    ((((app.qtySick1Y*(app.sickPercent1Group/100))+(app.qtySick1Y*(app.sickPercent2Group/100)))*app.gdpPerHabitant*(1-0.1233)))
//    +
    ((app.priceXeomin*4)*(12/3)*(app.qtySick1Y))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*25*(1-0.1233)*app.qtySick1Y)
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick1Y*12*(1-0.1233f))
    +
    (24029/5/((1)?1:4)*((0)?1:(1?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((0)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    
    self.none_total_ALL_1Y =
//    ((((app.qtySick1Y*(app.sickPercent1Group/100))+(app.qtySick1Y*(app.sickPercent2Group/100)))*app.gdpPerHabitant*(1-0.1233)))
//    +
    0
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*25*(1-0.0333)*app.qtySick1Y)
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick1Y*12*(1-0.0333f))
    +
    0
    +
    ((177793+32287)*((0)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    [self curChanged:self];
    [self modChanged:self];

}

- (IBAction)curChanged:(id)sender {
    
    // update total tabel
    int curTotal = self.curBotoxSlider.value + self.curDisportSlider.value + self.curXeominSlider.value + self.curNoneSlider.value;
    UISlider *slider = (UISlider *)sender;
    
    if (slider == self.curDisportSlider) {
        self.curBotoxSlider.value = (100-(int)self.curDisportSlider.value-(int)self.curXeominSlider.value-(int)self.curNoneSlider.value);
        curTotal -= self.curBotoxSlider.value;
        if (curTotal > 0) {
            self.curXeominSlider.value = (100-(int)self.curDisportSlider.value-(int)self.curBotoxSlider.value-(int)self.curNoneSlider.value);
            curTotal -= self.curXeominSlider.value;
        }
        if (curTotal > 0) {
            self.curNoneSlider.value = (100-(int)self.curDisportSlider.value-(int)self.curXeominSlider.value-(int)self.curBotoxSlider.value);
            curTotal -= self.curNoneSlider.value;
        }
    }
    if (slider == self.curBotoxSlider) {
        self.curXeominSlider.value = (100-(int)self.curBotoxSlider.value-(int)self.curDisportSlider.value-(int)self.curNoneSlider.value);
        curTotal -= self.curXeominSlider.value;
        if (curTotal > 0) {
            self.curNoneSlider.value = (100-(int)self.curXeominSlider.value-(int)self.curBotoxSlider.value-(int)self.curDisportSlider.value);
            curTotal -= self.curNoneSlider.value;
        }
        if (curTotal > 0) {
            self.curDisportSlider.value = (100-(int)self.curNoneSlider.value-(int)self.curXeominSlider.value-(int)self.curBotoxSlider.value);
            curTotal -= self.curDisportSlider.value;
        }
    }
    if (slider == self.curXeominSlider) {
        self.curNoneSlider.value = (100-(int)self.curBotoxSlider.value-(int)self.curXeominSlider.value-(int)self.curDisportSlider.value);
        curTotal -= self.curNoneSlider.value;
        if (curTotal > 0) {
            self.curDisportSlider.value = (100-(int)self.curNoneSlider.value-(int)self.curXeominSlider.value-(int)self.curBotoxSlider.value);
            curTotal -= self.curDisportSlider.value;
        }
        if (curTotal > 0) {
            self.curBotoxSlider.value = (100-(int)self.curDisportSlider.value-(int)self.curXeominSlider.value-(int)self.curNoneSlider.value);
            curTotal -= self.curBotoxSlider.value;
        }
    }
    if (slider == self.curNoneSlider) {
        self.curDisportSlider.value = (100-(int)self.curBotoxSlider.value-(int)self.curXeominSlider.value-(int)self.curNoneSlider.value);
        curTotal -= self.curDisportSlider.value;
        if (curTotal > 0) {
            self.curBotoxSlider.value = (100-(int)self.curDisportSlider.value-(int)self.curXeominSlider.value-(int)self.curNoneSlider.value);
            curTotal -= self.curBotoxSlider.value;
        }
        if (curTotal > 0) {
            self.curXeominSlider.value = (100-(int)self.curDisportSlider.value-(int)self.curNoneSlider.value-(int)self.curBotoxSlider.value);
            curTotal -= self.curXeominSlider.value;
        }
    }
    
    
    
    self.curDisportPercent.text = [NSString stringWithFormat:@"%d %%", (int)self.curDisportSlider.value];
    self.curBotoxPercent.text = [NSString stringWithFormat:@"%d %%", (int)self.curBotoxSlider.value];
    self.curXeominPercent.text = [NSString stringWithFormat:@"%d %%", (int)self.curXeominSlider.value];
    self.curNonePercent.text = [NSString stringWithFormat:@"%d %%", (int)self.curNoneSlider.value];
    //if (curTotal == 100) {
        self.curParts.text = [NSString stringWithFormat:@"Сумма долей = %d%% Все верно!", 100];
    //} else {
        //self.curParts.text = [NSString stringWithFormat:@"Сумма долей не равна 100%%. Текущая сумма равна %d%%!", curTotal];
    //}
    
    self.curValue =
          (self.disport_total_ALL_1Y*self.curDisportSlider.value)/100
        + (self.botox_total_ALL_1Y*self.curBotoxSlider.value)/100
        + (self.xeomin_total_ALL_1Y*self.curXeominSlider.value)/100
        + (self.none_total_ALL_1Y*self.curNoneSlider.value)/100;

    self.curTotalValue.text = MONEY_NUMBER(self.curValue);
    
    self.resultLabel.text = MONEY_NUMBER(self.curValue-self.modValue);
    [self preparePie1];
}

- (IBAction)modChanged:(id)sender {

    int modTotal = self.modBotoxSlider.value + self.modDisportSlider.value + self.modXeominSlider.value + self.modNoneSlider.value;
    
    UISlider *slider = (UISlider *)sender;
    
    if (slider == self.modDisportSlider) {
        self.modBotoxSlider.value = (100-(int)self.modDisportSlider.value-(int)self.modXeominSlider.value-(int)self.modNoneSlider.value);
        modTotal -= self.modBotoxSlider.value;
        if (modTotal > 0) {
            self.modXeominSlider.value = (100-(int)self.modDisportSlider.value-(int)self.modBotoxSlider.value-(int)self.modNoneSlider.value);
            modTotal -= self.modXeominSlider.value;
        }
        if (modTotal > 0) {
            self.modNoneSlider.value = (100-(int)self.modDisportSlider.value-(int)self.modXeominSlider.value-(int)self.modBotoxSlider.value);
            modTotal -= self.modNoneSlider.value;
        }
    }
    if (slider == self.modBotoxSlider) {
        self.modXeominSlider.value = (100-(int)self.modBotoxSlider.value-(int)self.modDisportSlider.value-(int)self.modNoneSlider.value);
        modTotal -= self.modXeominSlider.value;
        if (modTotal > 0) {
            self.modNoneSlider.value = (100-(int)self.modXeominSlider.value-(int)self.modBotoxSlider.value-(int)self.modDisportSlider.value);
            modTotal -= self.modNoneSlider.value;
        }
        if (modTotal > 0) {
            self.modDisportSlider.value = (100-(int)self.modNoneSlider.value-(int)self.modXeominSlider.value-(int)self.modBotoxSlider.value);
            modTotal -= self.modDisportSlider.value;
        }
    }
    if (slider == self.modXeominSlider) {
        self.modNoneSlider.value = (100-(int)self.modBotoxSlider.value-(int)self.modXeominSlider.value-(int)self.modDisportSlider.value);
        modTotal -= self.modNoneSlider.value;
        if (modTotal > 0) {
            self.modDisportSlider.value = (100-(int)self.modNoneSlider.value-(int)self.modXeominSlider.value-(int)self.modBotoxSlider.value);
            modTotal -= self.modDisportSlider.value;
        }
        if (modTotal > 0) {
            self.modBotoxSlider.value = (100-(int)self.modDisportSlider.value-(int)self.modXeominSlider.value-(int)self.modNoneSlider.value);
            modTotal -= self.modBotoxSlider.value;
        }
    }
    if (slider == self.modNoneSlider) {
        self.modDisportSlider.value = (100-(int)self.modBotoxSlider.value-(int)self.modXeominSlider.value-(int)self.modNoneSlider.value);
        modTotal -= self.modDisportSlider.value;
        if (modTotal > 0) {
            self.modBotoxSlider.value = (100-(int)self.modDisportSlider.value-(int)self.modXeominSlider.value-(int)self.modNoneSlider.value);
            modTotal -= self.modBotoxSlider.value;
        }
        if (modTotal > 0) {
            self.modXeominSlider.value = (100-(int)self.modDisportSlider.value-(int)self.modNoneSlider.value-(int)self.modBotoxSlider.value);
            modTotal -= self.modXeominSlider.value;
        }
    }

    
    self.modDisportPercent.text = [NSString stringWithFormat:@"%d %%", (int)self.modDisportSlider.value];
    self.modBotoxPercent.text = [NSString stringWithFormat:@"%d %%", (int)self.modBotoxSlider.value];
    self.modXeominPercent.text = [NSString stringWithFormat:@"%d %%", (int)self.modXeominSlider.value];
    self.modNonePercent.text = [NSString stringWithFormat:@"%d %%", (int)self.modNoneSlider.value];
    //if (modTotal == 100) {
        self.modParts.text = [NSString stringWithFormat:@"Сумма долей = %d%% Все верно!", 100];
    //} else {
    //    self.modParts.text = [NSString stringWithFormat:@"Сумма долей не равна 100%%. Текущая сумма равна %d%%!", modTotal];
   // }
    
    self.modValue =
    (self.disport_total_ALL_1Y*self.modDisportSlider.value)/100
    + (self.botox_total_ALL_1Y*self.modBotoxSlider.value)/100
    + (self.xeomin_total_ALL_1Y*self.modXeominSlider.value)/100
    + (self.none_total_ALL_1Y*self.modNoneSlider.value)/100;
    
    self.modTotalValue.text = MONEY_NUMBER(self.modValue);

    self.resultLabel.text = MONEY_NUMBER(self.curValue-self.modValue);
    [self preparePie2];
}

-(CPTFill *)sliceFillForPieChart:(CPTPieChart *)pieChart recordIndex:(NSUInteger)index {
	CPTFill *fill = [CPTFill fillWithColor:[sets objectForKey:[NSString stringWithFormat:@"%d", index]]];
	return fill;
}

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    return pieData1.count;
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    if ([plot.identifier isEqual:@"cur"])
        return [pieData1 objectAtIndex:index];
	else
        return [pieData2 objectAtIndex:index];
}

@end
