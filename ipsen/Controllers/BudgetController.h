//
//  BudgetController.h
//  ipsen
//
//  Created by oles on 8/8/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BudgetController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *tabSelector;

- (IBAction)tabChanged:(id)sender;

@end
