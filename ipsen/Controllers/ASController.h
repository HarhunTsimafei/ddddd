//
//  ASController.h
//  ipsen
//
//  Created by oles on 7/18/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASController : UIViewController

@property (weak, nonatomic) IBOutlet UISlider *disportSlider;
@property (weak, nonatomic) IBOutlet UISlider *botoxSlider;
@property (weak, nonatomic) IBOutlet UISlider *xeominSlider;

- (IBAction)disportChanged:(id)sender;
- (IBAction)botoxChanged:(id)sender;
- (IBAction)xeominChanged:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *disportLabel;
@property (weak, nonatomic) IBOutlet UILabel *botoxLabel;
@property (weak, nonatomic) IBOutlet UILabel *xeominLabel;
@property (weak, nonatomic) IBOutlet UILabel *noneLabel;

@property (weak, nonatomic) IBOutlet UILabel *disportValue;
@property (weak, nonatomic) IBOutlet UILabel *botoxValue;
@property (weak, nonatomic) IBOutlet UILabel *xeominValue;

@end
