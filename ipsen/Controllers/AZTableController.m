//
//  AZTableController.m
//  ipsen
//
//  Created by oles on 7/16/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "AZTableController.h"

@interface AZTableController ()



@end

@implementation AZTableController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.switchOn = true;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUI) name:NOTIFICATION_UPDATE_UI object:nil];

    [self updateUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [self updateUI];
}

- (void)updateUI {
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.yearForCalc = app.yearForCalc;
    self.populationForCalc = app.populationForCalcApp;
    
    CGFloat year_popul = self.yearForCalc*self.populationForCalc;
    
    self.disportCostsToxinA.text = MONEY_NUMBER(app.disportCostsToxinA*year_popul);
    self.disportCostsMedicalHelp.text = MONEY_NUMBER(app.disportCostsMedicalHelp*year_popul);
    self.disportCostsRelaxMedicalHelp.text = MONEY_NUMBER(app.disportCostsRelaxMedicalHelp*year_popul);
    self.disportCostsPension.text = MONEY_NUMBER(app.disportCostsPension*year_popul);
    CGFloat costsOp = 0.0;
    CGFloat costsPlast = app.disportCostsPlastering*year_popul;
    switch (app.monthsForCalc) {
        case 4:
            self.disportCostsOperations.text = MONEY_NUMBER(app.disportFourMouthCostsOperations*year_popul);
            costsOp = app.disportFourMouthCostsOperations;
            costsPlast = 25637* self.populationForCalc;
            break;
        case 12:
            self.disportCostsOperations.text = MONEY_NUMBER(app.disportOneYearCostsOperations*year_popul);
            costsOp = app.disportOneYearCostsOperations;
            break;
        case 24:
            self.disportCostsOperations.text = MONEY_NUMBER(app.disportTwoYearsCostsOperations*year_popul);
            costsOp = app.disportTwoYearsCostsOperations;
            break;
            
        default:
            break;
    }
    self.disportCostsPlastering.text = MONEY_NUMBER(costsPlast);
    self.disportCostsCompensation.text = MONEY_NUMBER((self.switchOn)? app.disportCostsCompensation*year_popul:0);
    self.disportCostsAllowanceForKids.text = MONEY_NUMBER(app.disportCostsAllowanceForKids*year_popul);
    self.disportCostsSum.text = MONEY_NUMBER(app.disportCostsToxinA*year_popul+
                                             app.disportCostsMedicalHelp*year_popul+
                                             app.disportCostsRelaxMedicalHelp*year_popul+
                                             app.disportCostsPension*year_popul+
                                             costsOp*year_popul+
                                             costsPlast+
                                             ((self.switchOn)? app.disportCostsCompensation*year_popul:0)
                                             +app.disportCostsAllowanceForKids*year_popul);
    
    
    

    self.botoxCostsToxinA.text = MONEY_NUMBER(app.botoxCostsToxinA*year_popul);
    self.botoxCostsMedicalHelp.text = MONEY_NUMBER(app.botoxCostsMedicalHelp*year_popul);
    self.botoxCostsRelaxMedicalHelp.text = MONEY_NUMBER(app.botoxCostsRelaxMedicalHelp*year_popul);
    self.botoxCostsPension.text = MONEY_NUMBER(app.botoxCostsPension*year_popul);
    costsPlast = app.botoxCostsPlastering*year_popul;
    switch (app.monthsForCalc) {
            
        case 4:
            self.botoxCostsOperations.text = MONEY_NUMBER(app.botoxFourMouthCostsOperations*year_popul);
            costsOp = app.botoxFourMouthCostsOperations;
            costsPlast = 25637* self.populationForCalc;
            break;
        case 12:
            self.botoxCostsOperations.text = MONEY_NUMBER(app.botoxOneYearCostsOperations*year_popul);
            costsOp = app.botoxOneYearCostsOperations;
            break;
        case 24:
            self.botoxCostsOperations.text = MONEY_NUMBER(app.botoxTwoYearsCostsOperations*year_popul);
            costsOp = app.botoxTwoYearsCostsOperations;
            break;
            
        default:
            break;
    }
    self.botoxCostsPlastering.text = MONEY_NUMBER(costsPlast);
    self.botoxCostsCompensation.text = MONEY_NUMBER((self.switchOn)? app.botoxCostsCompensation*year_popul:0);
    self.botoxCostsAllowanceForKids.text = MONEY_NUMBER(app.botoxCostsAllowanceForKids*year_popul);
    self.botoxCostsSum.text = MONEY_NUMBER(app.botoxCostsToxinA*year_popul+
                                           app.botoxCostsMedicalHelp*year_popul+
                                           app.botoxCostsRelaxMedicalHelp*year_popul+
                                           app.botoxCostsPension*year_popul+
                                           costsOp*year_popul+
                                           costsPlast+
                                           ((self.switchOn)? app.botoxCostsCompensation*year_popul:0)
                                           +app.botoxCostsAllowanceForKids*year_popul);
    
    
    


    self.noneCostsToxinA.text = MONEY_NUMBER(app.noneCostsToxinA*year_popul);
    self.noneCostsMedicalHelp.text = MONEY_NUMBER(app.noneCostsMedicalHelp*year_popul);
    self.noneCostsRelaxMedicalHelp.text = MONEY_NUMBER(app.noneCostsRelaxMedicalHelp*year_popul);
    self.noneCostsPension.text = MONEY_NUMBER(app.noneCostsPension*year_popul);
    costsPlast = app.noneCostsPlastering*year_popul;
    switch (app.monthsForCalc) {
        case 4:
            self.noneCostsOperations.text = MONEY_NUMBER(app.noneFourMouthCostsOperations*year_popul);
            costsOp = app.noneFourMouthCostsOperations;
            costsPlast = 25637* self.populationForCalc;
            break;
        case 12:
            self.noneCostsOperations.text = MONEY_NUMBER(app.noneOneYearCostsOperations*year_popul);
            costsOp = app.noneOneYearCostsOperations;
            break;
        case 24:
            self.noneCostsOperations.text = MONEY_NUMBER(app.noneTwoYearsCostsOperations*year_popul);
            costsOp = app.noneTwoYearsCostsOperations;
            break;
            
        default:
            break;
    }
    self.noneCostsPlastering.text = MONEY_NUMBER(costsPlast);
    self.noneCostsCompensation.text = MONEY_NUMBER((self.switchOn)?app.noneCostsCompensation*year_popul:0);
    self.noneCostsAllowanceForKids.text = MONEY_NUMBER(app.noneCostsAllowanceForKids*year_popul);
    self.noneCostsSum.text = MONEY_NUMBER(lroundf(app.noneCostsToxinA*year_popul+
                                          app.noneCostsMedicalHelp*year_popul+
                                          app.noneCostsRelaxMedicalHelp*year_popul+
                                          app.noneCostsPension*year_popul+
                                          costsOp*year_popul+
                                          costsPlast+
                                          ((self.switchOn)?app.noneCostsCompensation*year_popul:0)
                                          +app.noneCostsAllowanceForKids*year_popul));
 

    [self.tableView reloadData];
   
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 10;
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
