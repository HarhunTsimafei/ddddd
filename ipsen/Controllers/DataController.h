//
//  DataController.h
//  ipsen
//
//  Created by oles on 7/15/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataController : UITableViewController
@property (weak, nonatomic) IBOutlet UIButton *regionButton;

@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UITextField *priceDisportField;
@property (weak, nonatomic) IBOutlet UITextField *priceBotoxField;

@property (weak, nonatomic) IBOutlet UITextField *sickChild;



/*
@property (weak, nonatomic) IBOutlet UILabel *gdpPerCapitaField;

@property (weak, nonatomic) IBOutlet UILabel *averageWage1Field;
@property (weak, nonatomic) IBOutlet UILabel *averageWage2Field;

@property (weak, nonatomic) IBOutlet UILabel *habitantsField;
@property (weak, nonatomic) IBOutlet UILabel *insultQtyLabel;

@property (weak, nonatomic) IBOutlet UILabel *qtySickField;
*/
@property (weak, nonatomic) IBOutlet UISwitch *switchLocal;
@property (weak, nonatomic) IBOutlet UISwitch *switchMoscow;
- (IBAction)switchChanged:(id)sender;
 
@end
