//
//  AZContainerController.h
//  ipsen
//
//  Created by oles on 7/16/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AZContainerController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;
@property (weak, nonatomic) IBOutlet UISegmentedControl *viewSwitcher;

- (IBAction)switchView:(id)sender;
- (IBAction)showFilter:(id)sender;

@end
