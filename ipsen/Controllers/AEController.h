//
//  AEController.h
//  ipsen
//
//  Created by oles on 7/17/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AEController : UIViewController

- (IBAction)tabChanged:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *tabSwitcher;

@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view3;


@end
