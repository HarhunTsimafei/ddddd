//
//  SideController.m
//  ipsen
//
//  Created by oles on 7/17/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "SideController.h"
#import "SideCompareController.h"
#import "SideStaticController.h"

@interface SideController ()<UIPageViewControllerDelegate, UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageController;
@property (strong, nonatomic) SideCompareController *compareController;
@property (strong, nonatomic) SideStaticController *staticController;

@property (nonatomic) long long disportSideCost;
@property (nonatomic) long long botoxSideCost;
@property (nonatomic) long long baklofenSideCost;

@end

@implementation SideController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self setupPageController];
    
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"];
//    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:filePath];
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSDictionary *models = [app.dict objectForKey:@"side"];

    NSDictionary * disportModel = [models objectForKey:@"disport"];
    NSDictionary *botoxModel = [models objectForKey:@"botox"];
    NSDictionary *xeominModel = [models objectForKey:@"baklofen"];

    NSNumber *disportCost = [disportModel objectForKey:@"cost"];
    NSNumber *botoxCost = [botoxModel objectForKey:@"cost"];
    NSNumber *baklofenCost = [xeominModel objectForKey:@"cost"];
    self.disportSideCost = disportCost.longLongValue;
    self.botoxSideCost = botoxCost.longLongValue;
    self.baklofenSideCost = baklofenCost.longLongValue;
    self.sideCost.text = MONEY_NUMBER(self.disportSideCost);

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma marks - Controllers

- (SideCompareController *)compareController {
    if (!_compareController) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _compareController = (SideCompareController *)[storyBoard instantiateViewControllerWithIdentifier:@"SideCompare"];
    }
    return _compareController;
}

- (SideStaticController *)staticController {
    if (!_staticController) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _staticController = (SideStaticController *)[storyBoard instantiateViewControllerWithIdentifier:@"SideStatic"];
    }
    return _staticController;
}


- (void)setupPageController {
    NSArray *viewControllers = @[self.staticController];
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.pageController.delegate = self;
    self.pageController.dataSource = self;
    
    // disable swipe
    for (UIScrollView *view in self.pageController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            view.scrollEnabled = NO;
        }
    }
    
    [self addChildViewController:self.pageController];
    [self.containerView addSubview:self.pageController.view];
    
    // Set the page view controller's bounds using an inset rect so that self's view is visible around the edges of the pages.
    CGRect pageViewRect = self.containerView.bounds;
    pageViewRect = CGRectMake(pageViewRect.origin.x, pageViewRect.origin.y, pageViewRect.size.width, pageViewRect.size.height);
    self.pageController.view.frame = pageViewRect;
    
    [self.pageController didMoveToParentViewController:self];
    
    // Add the page view controller's gesture recognizers to the book view controller's view so that the gestures are started more easily.
    self.view.gestureRecognizers = self.pageController.gestureRecognizers;
}

- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    UIViewController *currentViewController = self.pageController.viewControllers[0];
    NSArray *viewControllers = @[currentViewController];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    
    self.pageController.doubleSided = NO;
    return UIPageViewControllerSpineLocationMin;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    return self.staticController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    return self.compareController;
}


- (IBAction)changeTab:(id)sender {
    switch (self.tabSwitcher.selectedSegmentIndex) {
        case 0:
        {
            self.headerView.hidden = YES;
            self.headerView2.hidden = NO;
            self.staticController.selectedDrug = 0;
            [self.staticController.tableView reloadData];
            self.sideCost.text = MONEY_NUMBER(self.disportSideCost);
            NSArray *viewControllers = @[self.staticController];
            [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        }
            break;
        case 1:
        {
            self.headerView.hidden = YES;
            self.headerView2.hidden = NO;
            self.staticController.selectedDrug = 1;
            [self.staticController.tableView reloadData];
            self.sideCost.text = MONEY_NUMBER(self.botoxSideCost);
            NSArray *viewControllers = @[self.staticController];
            [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        }
            break;
        case 2:
        {
            self.headerView.hidden = YES;
            self.headerView2.hidden = NO;
            self.staticController.selectedDrug = 2;
            [self.staticController.tableView reloadData];
            self.sideCost.text = MONEY_NUMBER(self.baklofenSideCost);
            NSArray *viewControllers = @[self.staticController];
            [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        }
            break;
        case 3:
        {
            self.headerView.hidden = NO;
            self.headerView2.hidden = YES;
            NSArray *viewControllers = @[self.compareController];
            [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        }
            break;
            
        default:
            break;
    }
}

@end
