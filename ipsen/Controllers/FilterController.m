//
//  FilterController.m
//  ipsen
//
//  Created by oles on 7/16/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "FilterController.h"
#import "Config.h"

@implementation FilterController

- (IBAction)dismissWithResult:(id)sender {
    
    int command = ((UIView *)sender).tag;
    //[self dismissPopoverAnimated:YES];
//    [self.navigationController dismissViewControllerAnimated:YES completion:^{
//        //
//    }];
//    [self dismissViewControllerAnimated:YES completion:^{
//    }];
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    switch (command) {
        case 1:
        {
            app.populationForCalc = 1;
            app.monthsForCalc = 4;
        }
            break;
        case 2:
        {
            app.populationForCalc = 1;
            app.monthsForCalc = 12;
        }
            break;
        case 3:
        {
            app.populationForCalc = 1;
            app.monthsForCalc = 24;
        }
            break;
        case 4:
        {
            app.populationForCalc = app.habitants;
            app.monthsForCalc = 4;
        }
            break;
            
        case 5:
        {
            app.populationForCalc = app.habitants;
            app.monthsForCalc = 12;
        }
            break;
            
        case 6:
        {
            app.populationForCalc = app.habitants;
            app.monthsForCalc = 24;
        }
            break;
            
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DATA_CHANGED object:nil userInfo:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.containerView.layer.borderColor = [UIColor blackColor].CGColor;
}

@end
