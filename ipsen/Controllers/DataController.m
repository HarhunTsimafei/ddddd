//
//  DataController.m
//  ipsen
//
//  Created by oles on 7/15/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "DataController.h"

@interface DataController ()<UITextFieldDelegate>

@property (nonatomic, strong) UIPopoverController *popover;

@property (weak, nonatomic) IBOutlet UITextField *countCourses;
@property (weak, nonatomic) IBOutlet UITextField *mh;

@end

@implementation DataController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.tableView.tableHeaderView = self.headerView;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUI) name:NOTIFICATION_UPDATE_UI object:nil];

    [self updateLabels];
    
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    NSArray* regions = app.regions;
    NSDictionary *region = regions[0];
    
    NSNumber *child = [region objectForKey:@"sick_child"];
    app.child_sick = child.longValue;
    NSNumber *gdp = [region objectForKey:@"gdp"];
    app.child_sick = child.longValue;
    app.gdpPerHabitant = gdp.longValue;
    app.regionString = [region objectForKey:@"name"];

    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DATA_CHANGED object:nil userInfo:nil];

    [self keyboardWillHide:nil];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateUI {
    [self.popover dismissPopoverAnimated:YES];
    [self updateLabels];
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [self.regionButton setTitle:app.regionString forState:UIControlStateNormal];
}

- (void)updateLabels {
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.qtyInsult        = INSULT_KOEF*app.habitants;
    app.qtySick          = SICK_NOW*app.habitants;
    app.qtySick3M        = SICK_3M;
    app.qtySick1Y        = SICK_1Y;
    app.qtySick5Y        = SICK_5Y;
        
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    numberFormatter.usesGroupingSeparator = YES;
    [numberFormatter setMaximumFractionDigits:0];
    [numberFormatter setMinimumFractionDigits:0];
    //[numberFormatter setGroupingSeparator:@","];
    
    self.priceDisportField.text = [NSString stringWithFormat:@"%lld", app.priceDisport];
    self.priceBotoxField.text = [NSString stringWithFormat:@"%lld",app.priceBotox];
    
    if(app.currentRegion == 0)
        self.sickChild.text = [NSString stringWithFormat:@"%lld", app.sickInAllRegions];
    else
         self.sickChild.text = [NSString stringWithFormat:@"%lld", app.child_sick];
    /*
    self.gdpPerCapitaField.text = MONEY_NUMBER(app.gdpPerHabitant);
    self.averageWage1Field.text = MONEY_NUMBER(app.averageWage1);
    self.averageWage2Field.text = MONEY_NUMBER(app.averageWage2);
    
    self.habitantsField.text = [NSString stringWithFormat:@"%@", GROUPED_NUMBER(app.habitants)];
    
    self.insultQtyLabel.text = [NSString stringWithFormat:@"%@ человек", GROUPED_NUMBER(app.qtyInsult)];
    self.qtySickField.text = [NSString stringWithFormat:@"%@ человек", GROUPED_NUMBER(app.qtySick)];
    */
     
    /*
    self.sickPercent3GroupField.text = [NSString stringWithFormat:@"%.2f %%", app.sickPercent3Group];
    self.sickPercentNoneGroupField.text = [NSString stringWithFormat:@"%.2f %%", app.sickPercentNoneGroup];
    
    self.invPayment1GroupField.text = MONEY_NUMBER(app.invPayment1Group);
    self.invPayment2GroupField.text = MONEY_NUMBER(app.invPayment2Group);
    self.invPayment3GroupField.text = MONEY_NUMBER(app.invPayment3Group);
    self.invPaymentNoneGroupField.text = MONEY_NUMBER(app.invPaymentNoneGroup);
    
    self.monthlyPayment1GroupField.text = MONEY_NUMBER(app.monthlyPayment1Group);
    self.monthlyPayment2GroupField.text = MONEY_NUMBER(app.monthlyPayment2Group);
    self.monthlyPayment3GroupField.text = MONEY_NUMBER(app.monthlyPayment3Group);
    self.monthlyPaymentNoneGroupField.text = MONEY_NUMBER(app.monthlyPaymentNoneGroup);
    */
}

- (void)keyboardWillHide:(NSNotification *)notification {
    // recalc and change initial data
    
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    // save data
    NSString* localeSeparator = [[NSLocale currentLocale]
                                 objectForKey:NSLocaleGroupingSeparator];
    
    app.priceDisport = [self.priceDisportField.text stringByReplacingOccurrencesOfString:localeSeparator withString:@""].longLongValue;
    app.priceBotox = [self.priceBotoxField.text stringByReplacingOccurrencesOfString:localeSeparator withString:@""].longLongValue;
    
    [app calcExpensesAnysis];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 8;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.headerView;
}
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

///*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ShowRegion"]) {
        UIStoryboardPopoverSegue *popoverSegue = (UIStoryboardPopoverSegue *)segue;
        popoverSegue.popoverController.backgroundColor = [UIColor whiteColor];
        self.popover = popoverSegue.popoverController;
    }
}
- (IBAction)disportCostChange:(id)sender {
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    app.priceDisport = [self.priceDisportField.text floatValue];
    [app recountData];
}

- (IBAction)botoxCostChange:(id)sender {
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    app.priceBotox = [self.priceBotoxField.text floatValue];
    [app recountData];
}
- (IBAction)countDCPchange:(id)sender {
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    if (app.currentRegion == 0) {
        app.sickInAllRegions =[self.sickChild.text longLongValue];
    }

    [app recountData];
}

 //*/

//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    NSMutableString* mstring = [[textField text] mutableCopy];
//    
//    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc]init];
//    [numberFormatter setLocale:[NSLocale currentLocale]];
//    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
//    
//    NSString* localeSeparator = [[NSLocale currentLocale]
//                                 objectForKey:NSLocaleGroupingSeparator];
//    NSNumber* number = [numberFormatter numberFromString:[mstring
//                                                          stringByReplacingOccurrencesOfString:localeSeparator withString:@""]];
//    
//    [textField setText:[numberFormatter stringFromNumber:number]];
//    
//    
//
//}

- (IBAction)switchChanged:(id)sender {
    if (sender == self.switchLocal)
        self.switchMoscow.on = !self.switchLocal.on;
    else
        self.switchLocal.on = !self.switchMoscow.on;
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.writemh = [self.mh.text longLongValue];
    app.mh = self.switchLocal.on;
    [app recountData];
}

@end
