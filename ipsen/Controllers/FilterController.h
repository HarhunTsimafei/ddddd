//
//  FilterController.h
//  ipsen
//
//  Created by oles on 7/16/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterController : UIViewController

- (IBAction)dismissWithResult:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *containerView;

@end
