//
//  SummaryController.m
//  ipsen
//
//  Created by oles on 7/17/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "SummaryController.h"

@interface SummaryController ()

@end

@implementation SummaryController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUI) name:NOTIFICATION_UPDATE_UI object:nil];
    [self updateUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 18;
}


- (void)updateUI {
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    // disport
    
    long long disport_total_1_3M =
    (((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*5*(1-0.17)))/4)
    +
    ((app.priceDisport*2)*(3/3))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*(1-0.17))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
     +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
     +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*3*(1-0.17f))
    +
    (16538/5/((0)?1:4)*((1)?1:(0?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((1)?1:((app.qtySick3M))));
    
    long long disport_total_ALL_3M =
//    ((((app.qtySick3M*(app.sickPercent1Group/100))+(app.qtySick3M*(app.sickPercent2Group/100))))*app.gdpPerHabitant/4)
//    +
    ((app.priceDisport*2)*(3/3)*(app.qtySick3M))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*app.qtySick3M*(1-0.17f))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
     +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
     +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick3M*3*(1-0.17f))
    +
    (16538/5/(4)*((app.qtySick3M)))
    +
    ((177793+32287)*((0)?1:((app.qtySick3M))));
    
    long long disport_total_1_1Y =
    ((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*(1-0.17f)))
    +
    ((app.priceDisport*2)*(12/3))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*5*(1-0.17f))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
     +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
     +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*12*(1-0.17f))
    +
    (16538/5/((1)?1:4)*(1))
    +
    ((177793+32287)*(1));
    
    long long disport_total_ALL_1Y =
//    ((((app.qtySick1Y*(app.sickPercent1Group/100))+(app.qtySick1Y*(app.sickPercent2Group/100)))*app.gdpPerHabitant*(1-0.17f)))
//    +
    ((app.priceDisport*2)*(12/3)*(app.qtySick1Y))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*25*(1-0.17f)*app.qtySick1Y)
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
    +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
    +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick1Y*12*(1-0.17f))
    +
    (16538/5/((1)?1:4)*((0)?1:(app.qtySick1Y)))
    +
    ((177793+32287)*(((app.qtySick1Y))));
    
    // botox
    
    long long botox_total_1_3M =
    (((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*5*(1-0.127f)))/4)
    +
    ((app.priceBotox*3)*(3/3))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*(1-0.127f))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*3*(1-0.127f))
    +
    (22250/5/((0)?1:4)*((1)?1:(app.qtySick3M)))
    +
    ((177793+32287)*((1)?1:((app.qtySick3M))));
    
    long long botox_total_ALL_3M =
//    ((((app.qtySick3M*(app.sickPercent1Group/100))+(app.qtySick3M*(app.sickPercent2Group/100))))*app.gdpPerHabitant/4)
//    +
    ((app.priceBotox*3)*(3/3)*(app.qtySick3M))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*app.qtySick3M*(1-0.127f))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick3M*3*(1-0.127f))
    +
    (22250/5/(4)*((app.qtySick3M)))
    +
    ((177793+32287)*((0)?1:((app.qtySick3M))));
    
    long long botox_total_1_1Y =
    ((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*(1-0.127f)))
    +
    ((app.priceBotox*3)*(12/3))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*5*(1-0.127))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*12*(1-0.127f))
    +
    (22250/5/((1)?1:4)*((1)?1:(1?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((1)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    long long botox_total_ALL_1Y =
//    ((((app.qtySick1Y*(app.sickPercent1Group/100))+(app.qtySick1Y*(app.sickPercent2Group/100)))*app.gdpPerHabitant*(1-0.127f)))
//    +
    ((app.priceBotox*3)*(12/3)*(app.qtySick1Y))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*25*(1-0.127f)*app.qtySick1Y)
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick1Y*12*(1-0.127f))
    +
    (22250/5/((1)?1:4)*((0)?1:(1?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((0)?1:((1?app.qtySick1Y:app.qtySick3M))));

    
    // xeomin
    long long xeomin_total_1_3M =
    (((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*5*(1-0.1233)))/4)
    +
    ((app.priceXeomin*4)*(3/3))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*(1-0.1233))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*3*(1-0.1233f))
    +
    (24029/5/((0)?1:4)*((1)?1:(0?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((1)?1:((0?app.qtySick1Y:app.qtySick3M))));
    
    long long xeomin_total_ALL_3M =
//    ((((app.qtySick3M*(app.sickPercent1Group/100))+(app.qtySick3M*(app.sickPercent2Group/100))))*app.gdpPerHabitant/4)
//    +
    ((app.priceXeomin*4)*(3/3)*(app.qtySick3M))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*app.qtySick3M*(1-0.1233f))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick3M*3*(1-0.1233f))
    +
    (24029/5/(4)*((app.qtySick3M)))
    +
    ((177793+32287)*((0)?1:((0?app.qtySick1Y:app.qtySick3M))));
    
    long long xeomin_total_1_1Y =
    ((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*(1-0.1233f)))
    +
    ((app.priceXeomin*4)*(12/3))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*5*(1-0.1233f))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*12*(1-0.1233f))
    +
    (24029/5/((1)?1:4)*((1)?1:(1?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((1)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    long long xeomin_total_ALL_1Y =
//    ((((app.qtySick1Y*(app.sickPercent1Group/100))+(app.qtySick1Y*(app.sickPercent2Group/100)))*app.gdpPerHabitant*(1-0.1233)))
//    +
    ((app.priceXeomin*4)*(12/3)*(app.qtySick1Y))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*25*(1-0.1233f)*app.qtySick1Y)
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick1Y*12*(1-0.1233f))
    +
    (24029/5/((1)?1:4)*((0)?1:(1?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((0)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    // none
    long long none_total_1_3M =
    (((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*5*(1-0.0333f)))/4)
    +
    0
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*(1-0.0333f))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*3*(1-0.0333f))
    +
    0
    +
    ((177793+32287)*((1)?1:((0?app.qtySick1Y:app.qtySick3M))));
    
    long long none_total_ALL_3M =
//    (((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*5*(1-0.0333f)))/4)
//    +
    0
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*app.qtySick3M*(1-0.0333f))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick3M*3*(1-0.0333f))
    +
    0
    +
    ((177793+32287)*((0)?1:((0?app.qtySick1Y:app.qtySick3M))));
    
    long long none_total_1_1Y =
    ((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*(1-0.0333f)))
    +
    0
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*5*(1-0.0333f))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*12*(1-0.0333f))
    +
    0
    +
    ((177793+32287)*((1)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    long long none_total_ALL_1Y =
//    ((((app.qtySick1Y*(app.sickPercent1Group/100))+(app.qtySick1Y*(app.sickPercent2Group/100)))*app.gdpPerHabitant*(1-0.0333f)))
//    +
    0
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*25*(1-0.0333f)*app.qtySick1Y)
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick1Y*12*(1-0.0333f))
    +
    0
    +
    ((177793+32287)*((0)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    /*
    float botoxScale = (app.ashfortDisport-app.ashfortBotox)/app.ashfortDisport*100;
    self.botoxScaleValue.text = [NSString stringWithFormat:@"%.0f %%", ABS(botoxScale)];
    self.botoxScaleSign.text = (botoxScale>0)?@"Уменьшится":@"Увеличится";
    self.botoxScaleSign.textColor = (botoxScale>0)?GREEN_COLOR:RED_COLOR;

    float xeominScale = (app.ashfortDisport-app.ashfortXeomin)/app.ashfortDisport*100;
    self.xeominScaleValue.text = [NSString stringWithFormat:@"%.0f %%", ABS(xeominScale)];
    self.xeominScaleSign.text = (xeominScale>0)?@"Уменьшится":@"Увеличится";
    self.xeominScaleSign.textColor = (xeominScale>0)?GREEN_COLOR:RED_COLOR;

    float noneScale = (app.ashfortDisport-app.ashfortNone)/app.ashfortDisport*100;
    self.noneScaleValue.text = [NSString stringWithFormat:@"%.0f %%", ABS(noneScale)];
    self.noneScaleSign.text = (noneScale>0)?@"Уменьшится":@"Увеличится";
    self.noneScaleSign.textColor = (noneScale>0)?GREEN_COLOR:RED_COLOR;

    
    // botox
    long long botoxScale_1_3M = (botox_total_1_3M-disport_total_1_3M);
    self.botoxExpOne3MValue.text = MONEY_NUMBER(ABS(botoxScale_1_3M));
    self.botoxExpOne3MSign.text = (botoxScale_1_3M>0)?@"Уменьшатся":@"Увеличатся";
    self.botoxExpOne3MSign.textColor = (botoxScale_1_3M>0)?GREEN_COLOR:RED_COLOR;

    long long botoxScale_ALL_3M = (botox_total_ALL_3M-disport_total_ALL_3M);
    self.botoxExpAll3MValue.text = MONEY_NUMBER(ABS(botoxScale_ALL_3M));
    self.botoxExpAll3MSign.text = (botoxScale_ALL_3M>0)?@"Уменьшатся":@"Увеличатся";
    self.botoxExpAll3MSign.textColor = (botoxScale_ALL_3M>0)?GREEN_COLOR:RED_COLOR;

    long long botoxScale_1_1Y = (botox_total_1_1Y-disport_total_1_1Y);
    self.botoxExpOne1YValue.text = MONEY_NUMBER(ABS(botoxScale_1_1Y));
    self.botoxExpOne1YSign.text = (botoxScale_1_1Y>0)?@"Уменьшатся":@"Увеличатся";
    self.botoxExpOne1YSign.textColor = (botoxScale_1_1Y>0)?GREEN_COLOR:RED_COLOR;
    
    long long botoxScale_ALL_1Y = (botox_total_ALL_1Y-disport_total_ALL_1Y);
    self.botoxExpAll1YValue.text = MONEY_NUMBER(ABS(botoxScale_ALL_1Y));
    self.botoxExpAll1YSign.text = (botoxScale_ALL_1Y>0)?@"Уменьшатся":@"Увеличатся";
    self.botoxExpAll1YSign.textColor = (botoxScale_ALL_1Y>0)?GREEN_COLOR:RED_COLOR;

    // xeomin
    long long xeominScale_1_3M = (xeomin_total_1_3M-disport_total_1_3M);
    self.xeominExpOne3MValue.text = MONEY_NUMBER(ABS(xeominScale_1_3M));
    self.xeominExpOne3MSign.text = (xeominScale_1_3M>0)?@"Уменьшатся":@"Увеличатся";
    self.xeominExpOne3MSign.textColor = (xeominScale_1_3M>0)?GREEN_COLOR:RED_COLOR;
    
    long long xeominScale_ALL_3M = (xeomin_total_ALL_3M-disport_total_ALL_3M);
    self.xeominExpAll3MValue.text = MONEY_NUMBER(ABS(xeominScale_ALL_3M));
    self.xeominExpAll3MSign.text = (xeominScale_ALL_3M>0)?@"Уменьшатся":@"Увеличатся";
    self.xeominExpAll3MSign.textColor = (xeominScale_ALL_3M>0)?GREEN_COLOR:RED_COLOR;
    
    long long xeominScale_1_1Y = (xeomin_total_1_1Y-disport_total_1_1Y);
    self.xeominExpOne1YValue.text = MONEY_NUMBER(ABS(xeominScale_1_1Y));
    self.xeominExpOne1YSign.text = (xeominScale_1_1Y>0)?@"Уменьшатся":@"Увеличатся";
    self.xeominExpOne1YSign.textColor = (xeominScale_1_1Y>0)?GREEN_COLOR:RED_COLOR;
    
    long long xeominScale_ALL_1Y = (xeomin_total_ALL_1Y-disport_total_ALL_1Y);
    self.xeominExpAll1YValue.text = MONEY_NUMBER(ABS(xeominScale_ALL_1Y));
    self.xeominExpAll1YSign.text = (xeominScale_ALL_1Y>0)?@"Уменьшатся":@"Увеличатся";
    self.xeominExpAll1YSign.textColor = (xeominScale_ALL_1Y>0)?GREEN_COLOR:RED_COLOR;

    // none
    long long noneScale_1_3M = (none_total_1_3M-disport_total_1_3M);
    self.noneExpOne3MValue.text = MONEY_NUMBER(ABS(noneScale_1_3M));
    self.noneExpOne3MSign.text = (noneScale_1_3M>0)?@"Уменьшатся":@"Увеличатся";
    self.noneExpOne3MSign.textColor = (noneScale_1_3M>0)?GREEN_COLOR:RED_COLOR;
    
    long long noneScale_ALL_3M = (none_total_ALL_3M-disport_total_ALL_3M);
    self.noneExpAll3MValue.text = MONEY_NUMBER(ABS(noneScale_ALL_3M));
    self.noneExpAll3MSign.text = (noneScale_ALL_3M>0)?@"Уменьшатся":@"Увеличатся";
    self.noneExpAll3MSign.textColor = (noneScale_ALL_3M>0)?GREEN_COLOR:RED_COLOR;
    
    long long noneScale_1_1Y = (none_total_1_1Y-disport_total_1_1Y);
    self.noneExpOne1YValue.text = MONEY_NUMBER(ABS(noneScale_1_1Y));
    self.noneExpOne1YSign.text = (noneScale_1_1Y>0)?@"Уменьшатся":@"Увеличатся";
    self.noneExpOne1YSign.textColor = (noneScale_1_1Y>0)?GREEN_COLOR:RED_COLOR;
    
    long long noneScale_ALL_1Y = (none_total_ALL_1Y-disport_total_ALL_1Y);
    self.noneExpAll1YValue.text = MONEY_NUMBER(ABS(noneScale_ALL_1Y));
    self.noneExpAll1YSign.text = (noneScale_ALL_1Y>0)?@"Уменьшатся":@"Увеличатся";
    self.noneExpAll1YSign.textColor = (noneScale_ALL_1Y>0)?GREEN_COLOR:RED_COLOR;
    
     */
     
    [self calcEff];
    
}

- (void)calcEff {
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    long long disport_total_1_3M =
    (((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*5*(1-0.17)))/4)
    +
    ((app.priceDisport*2)*(3/3))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*(1-0.17))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*3*(1-0.17f))
    +
    (16538/5/((0)?1:4)*((1)?1:(0?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((1)?1:((app.qtySick3M))));
    
    long long disport_total_1_1Y =
    ((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*(1-0.17)))
    +
    ((app.priceDisport*2)*(12/3))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*5*(1-0.17))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*12*(1-0.17f))
    +
    (16538/5/((1)?1:4)*(1))
    +
    ((177793+32287)*(1));
    
    long long botox_total_1_3M =
    (((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*5*(1-0.127)))/4)
    +
    ((app.priceBotox*3)*(3/3))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*(1-0.127))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*3*(1-0.127f))
    +
    (22250/5/((0)?1:4)*((1)?1:(app.qtySick3M)))
    +
    ((177793+32287)*((1)?1:((app.qtySick3M))));
    
    long long botox_total_1_1Y =
    ((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*(1-0.127)))
    +
    ((app.priceBotox*3)*(12/3))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*5*(1-0.127))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*12*(1-0.127f))
    +
    (22250/5/((1)?1:4)*((1)?1:(1?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((1)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    long long xeomin_total_1_3M =
    (((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*5*(1-0.1233)))/4)
    +
    ((app.priceXeomin*4)*(3/3))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*(1-0.1233))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*3*(1-0.1233f))
    +
    (24029/5/((0)?1:4)*((1)?1:(0?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((1)?1:((0?app.qtySick1Y:app.qtySick3M))));
    
    long long xeomin_total_1_1Y =
    ((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*(1-0.1233)))
    +
    ((app.priceXeomin*4)*(12/3))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*5*(1-0.1233))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*12*(1-0.1233f))
    +
    (24029/5/((1)?1:4)*((1)?1:(1?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((1)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    long long none_total_1_3M =
    (((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*5*(1-0.0333)))/4)
    +
    0
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*(1-0.0333))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*3*(1-0.0333f))
    +
    0
    +
    ((177793+32287)*((1)?1:((0?app.qtySick1Y:app.qtySick3M))));
    
    long long none_total_1_1Y =
    ((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*(1-0.0333)))
    +
    0
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*5*(1-0.0333))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*12*(1-0.0333f))
    +
    0
    +
    ((177793+32287)*((1)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    
    double exp_eff_botox_3M = (botox_total_1_3M/app.ashfortBotox)-(disport_total_1_3M/app.ashfortDisport);
    double exp_eff_xeomin_3M = (xeomin_total_1_3M/app.ashfortXeomin)-(disport_total_1_3M/app.ashfortDisport);
    double exp_eff_none_3M = (none_total_1_3M/app.ashfortNone)-(disport_total_1_3M/app.ashfortDisport);
    double exp_eff_botox_1Y = (botox_total_1_1Y/app.ashfortBotox)-(disport_total_1_1Y/app.ashfortDisport);
    double exp_eff_xeomin_1Y = (xeomin_total_1_1Y/app.ashfortXeomin)-(disport_total_1_1Y/app.ashfortDisport);
    double exp_eff_none_1Y = (none_total_1_1Y/app.ashfortNone)-(disport_total_1_1Y/app.ashfortDisport);

    /*
    
    self.botoxExpEffValue3M.text = MONEY_NUMBER(ABS(exp_eff_botox_3M));
    self.botoxExpEffSign3M.text = (exp_eff_botox_3M>0)?@"Уменьшится":@"Увеличится";
    self.botoxExpEffSign3M.textColor = (exp_eff_botox_3M>0)?GREEN_COLOR:RED_COLOR;

    self.botoxExpEff1YValue.text = MONEY_NUMBER(ABS(exp_eff_botox_1Y));
    self.botoxExpEff1YSign.text = (exp_eff_botox_1Y>0)?@"Уменьшится":@"Увеличится";
    self.botoxExpEff1YSign.textColor = (exp_eff_botox_1Y>0)?GREEN_COLOR:RED_COLOR;

    self.xeominExpEff3MValue.text = MONEY_NUMBER(ABS(exp_eff_xeomin_3M));
    self.xeominExpEff3MSign.text = (exp_eff_xeomin_3M>0)?@"Уменьшится":@"Увеличится";
    self.xeominExpEff3MSign.textColor = (exp_eff_xeomin_3M>0)?GREEN_COLOR:RED_COLOR;

    self.xeominExpEff1YValue.text = MONEY_NUMBER(ABS(exp_eff_xeomin_1Y));
    self.xeominExpEff1YSign.text = (exp_eff_xeomin_1Y>0)?@"Уменьшится":@"Увеличится";
    self.xeominExpEff1YSign.textColor = (exp_eff_xeomin_1Y>0)?GREEN_COLOR:RED_COLOR;

    self.noneExpEff3MValue.text = MONEY_NUMBER(ABS(exp_eff_none_3M));
    self.noneExpEff3MSign.text = (exp_eff_none_3M>0)?@"Уменьшится":@"Увеличится";
    self.noneExpEff3MSign.textColor = (exp_eff_none_3M>0)?GREEN_COLOR:RED_COLOR;
    
    self.noneExpEff1YValue.text = MONEY_NUMBER(ABS(exp_eff_none_1Y));
    self.noneExpEff1YSign.text = (exp_eff_none_1Y>0)?@"Уменьшится":@"Увеличится";
    self.noneExpEff1YSign.textColor = (exp_eff_none_1Y>0)?GREEN_COLOR:RED_COLOR;
    */
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
}

@end
