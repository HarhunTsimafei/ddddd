//
//  AnalisysController.m
//  ipsen
//
//  Created by oles on 7/15/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "AnalisysController.h"

@interface AnalisysController ()<UIPageViewControllerDelegate, UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageController;
@property (strong, nonatomic) AZContainerController *azContainerController;
@property (strong, nonatomic) AEController *aeController;
@property (strong, nonatomic) AZEController *azeController;
@property (strong, nonatomic) ASController *asController;
@property (strong, nonatomic) BudgetController *budgetController;

@end

@implementation AnalisysController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self setupPageController];
    switch (self.currentAnalysis) {
        case 1:
            [self doAZ:self];
            break;
        case 2:
            [self doAS:self];
            break;
        case 3:
            [self doAE:self];
            break;
        case 4:
            [self doAZE:self];
            break;
        case 5:
            [self doBudget:self];
            break;
            
        default:
            break;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

#pragma marks - Controllers

- (AZContainerController *)azContainerController {
    if (!_azContainerController) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _azContainerController = (AZContainerController *)[storyBoard instantiateViewControllerWithIdentifier:@"AZContainer"];
    }
    return _azContainerController;
}

- (AEController *)aeController {
    if (!_aeController) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _aeController = (AEController *)[storyBoard instantiateViewControllerWithIdentifier:@"AE"];
    }
    return _aeController;
}

- (AZEController *)azeController {
    if (!_azeController) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _azeController = (AZEController *)[storyBoard instantiateViewControllerWithIdentifier:@"AZE"];
    }
    return _azeController;
}

- (ASController *)asController {
    if (!_asController) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _asController = (ASController *)[storyBoard instantiateViewControllerWithIdentifier:@"AS"];
    }
    return _asController;
}

- (BudgetController *)budgetController {
    if (!_budgetController) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _budgetController = (BudgetController *)[storyBoard instantiateViewControllerWithIdentifier:@"Budget"];
    }
    return _budgetController;
}

- (void)setupPageController {
    NSArray *viewControllers = @[self.azContainerController];
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.pageController.delegate = self;
    self.pageController.dataSource = self;
    
    // disable swipe
    for (UIScrollView *view in self.pageController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            view.scrollEnabled = NO;
        }
    }
    
    [self addChildViewController:self.pageController];
    [self.containerView addSubview:self.pageController.view];
    
    // Set the page view controller's bounds using an inset rect so that self's view is visible around the edges of the pages.
    CGRect pageViewRect = self.containerView.bounds;
    pageViewRect = CGRectMake(pageViewRect.origin.x, pageViewRect.origin.y, pageViewRect.size.width, pageViewRect.size.height);
    self.pageController.view.frame = pageViewRect;
    
    [self.pageController didMoveToParentViewController:self];
    
    // Add the page view controller's gesture recognizers to the book view controller's view so that the gestures are started more easily.
    self.view.gestureRecognizers = self.pageController.gestureRecognizers;
}

- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    UIViewController *currentViewController = self.pageController.viewControllers[0];
    NSArray *viewControllers = @[currentViewController];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    
    self.pageController.doubleSided = NO;
    return UIPageViewControllerSpineLocationMin;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    return self.azContainerController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    return self.azContainerController;
}

- (IBAction)doAZ:(id)sender {
    NSArray *viewControllers = @[self.azContainerController];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.navigationItem.title = @"Анализ затрат";
    
    self.buttonAZ.selected = YES;
    self.buttonEff.selected = NO;
    self.buttonExpEff.selected = NO;
    self.buttonBudget.selected = NO;
    self.buttonSens.selected = NO;
}

- (IBAction)doAE:(id)sender {
    NSArray *viewControllers = @[self.aeController];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.navigationItem.title = @"Анализ эффективности";
    
    self.buttonAZ.selected = NO;
    self.buttonEff.selected = YES;
    self.buttonExpEff.selected = NO;
    self.buttonBudget.selected = NO;
    self.buttonSens.selected = NO;
}

- (IBAction)doAZE:(id)sender {
    NSArray *viewControllers = @[self.azeController];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.navigationItem.title = @"Анализ затраты-эффективность";
    
    self.buttonAZ.selected = NO;
    self.buttonEff.selected = NO;
    self.buttonExpEff.selected = YES;
    self.buttonBudget.selected = NO;
    self.buttonSens.selected = NO;
}

- (IBAction)doBudget:(id)sender {
    NSArray *viewControllers = @[self.budgetController];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.navigationItem.title = @"Анализ влияния на бюджет";
    
    self.buttonAZ.selected = NO;
    self.buttonEff.selected = NO;
    self.buttonExpEff.selected = NO;
    self.buttonBudget.selected = YES;
    self.buttonSens.selected = NO;
}

- (IBAction)doAS:(id)sender {
    NSArray *viewControllers = @[self.asController];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.navigationItem.title = @"Анализ чувствительности";
    
    self.buttonAZ.selected = NO;
    self.buttonEff.selected = NO;
    self.buttonExpEff.selected = NO;
    self.buttonBudget.selected = NO;
    self.buttonSens.selected = YES;
}

@end
