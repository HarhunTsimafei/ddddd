//
//  MainController.m
//  ipsen
//
//  Created by oles on 7/15/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "MainController.h"

@interface MainController ()<UIPageViewControllerDelegate, UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageController;
@property (strong, nonatomic) DataController *dataController;
@property (strong, nonatomic) SummaryController *summaryController;
//@property (strong, nonatomic) StandartsController *standartsController;
@property (strong, nonatomic) SideController *sideController;
@property (strong, nonatomic) SourcesController *sourcesController;

@property (nonatomic) int currentAnalysis;

@end

@implementation MainController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self setupPageController];
    self.titleView.hidden = YES;
    self.buttonData.selected = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma marks - Controllers

- (DataController *)dataController {
    if (!_dataController) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _dataController = (DataController *)[storyBoard instantiateViewControllerWithIdentifier:@"Data"];
    }
    return _dataController;
}

- (SummaryController *)summaryController {
    if (!_summaryController) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _summaryController = (SummaryController *)[storyBoard instantiateViewControllerWithIdentifier:@"Summary"];
    }
    return _summaryController;
}

- (SideController *)sideController {
    if (!_sideController) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _sideController = (SideController *)[storyBoard instantiateViewControllerWithIdentifier:@"Side"];
    }
    return _sideController;
}


- (SourcesController *)sourcesController {
    if (!_sourcesController) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _sourcesController = (SourcesController *)[storyBoard instantiateViewControllerWithIdentifier:@"Sources"];
    }
    return _sourcesController;
}


- (void)setupPageController {
    NSArray *viewControllers = @[self.dataController];
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.pageController.delegate = self;
    self.pageController.dataSource = self;
    
    // disable swipe
    for (UIScrollView *view in self.pageController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            view.scrollEnabled = NO;
        }
    }
    
    [self addChildViewController:self.pageController];
    [self.containerView addSubview:self.pageController.view];
    
    // Set the page view controller's bounds using an inset rect so that self's view is visible around the edges of the pages.
    CGRect pageViewRect = self.containerView.bounds;
    pageViewRect = CGRectMake(pageViewRect.origin.x, pageViewRect.origin.y, pageViewRect.size.width, pageViewRect.size.height);
    self.pageController.view.frame = pageViewRect;
    
    [self.pageController didMoveToParentViewController:self];
    
    // Add the page view controller's gesture recognizers to the book view controller's view so that the gestures are started more easily.
    self.view.gestureRecognizers = self.pageController.gestureRecognizers;
}

- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    UIViewController *currentViewController = self.pageController.viewControllers[0];
    NSArray *viewControllers = @[currentViewController];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    
    self.pageController.doubleSided = NO;
    return UIPageViewControllerSpineLocationMin;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    return self.dataController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    return self.dataController;
}

- (IBAction)doAnalysis:(id)sender {
    //[self performSegueWithIdentifier:@"ShowAnalysis" sender:self];
    self.buttonData.selected = NO;
    self.buttonAnalysis.selected = YES;
    self.buttonAdd.selected = NO;
    self.buttonSummary.selected = NO;
    self.titleView.hidden = NO;
}

- (IBAction)doStandarts:(id)sender {
    [self performSegueWithIdentifier:@"ShowAdd" sender:self];
}

//- (IBAction)doSideEffects:(id)sender {
//    NSArray *viewControllers = @[self.sideController];
//    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
//    self.navigationItem.title = @"Сторонние еффекты";
//}

- (IBAction)doSummary:(id)sender {
    NSArray *viewControllers = @[self.summaryController];
        [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.navigationItem.title = @"Выводы";

    self.buttonData.selected = NO;
    self.buttonAnalysis.selected = NO;
    self.buttonAdd.selected = NO;
    self.buttonSummary.selected = YES;
    self.titleView.hidden = YES;
}

- (IBAction)doData:(id)sender {
    NSArray *viewControllers = @[self.dataController];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.navigationItem.title = @"Данные";
    
    self.buttonData.selected = YES;
    self.buttonAnalysis.selected = NO;
    self.buttonAdd.selected = NO;
    self.buttonSummary.selected = NO;
    self.titleView.hidden = YES;
}

//- (IBAction)doSources:(id)sender {
//    NSArray *viewControllers = @[self.sourcesController];
//    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
//    self.navigationItem.title = @"Справочные материалы";
//}

- (IBAction)selectAZ:(id)sender {
    self.currentAnalysis = 1;
    [self performSegueWithIdentifier:@"ShowAnalysis" sender:self];
}

- (IBAction)selectSens:(id)sender {
    self.currentAnalysis = 2;
    [self performSegueWithIdentifier:@"ShowAnalysis" sender:self];
}

- (IBAction)selectEff:(id)sender {
    self.currentAnalysis = 3;
    [self performSegueWithIdentifier:@"ShowAnalysis" sender:self];
}

- (IBAction)selectExpEff:(id)sender {
    self.currentAnalysis = 4;
    [self performSegueWithIdentifier:@"ShowAnalysis" sender:self];
}

- (IBAction)selectBudget:(id)sender {
    self.currentAnalysis = 5;
    [self performSegueWithIdentifier:@"ShowAnalysis" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowAnalysis"]) {
        AnalisysController *controller = (AnalisysController *)segue.destinationViewController;
        controller.currentAnalysis = self.currentAnalysis;
    }
}

@end
