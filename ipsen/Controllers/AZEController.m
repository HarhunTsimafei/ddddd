//
//  AZEController.m
//  ipsen
//
//  Created by oles on 7/17/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "AZEController.h"

@interface AZEController ()<CPTPlotDataSource, CPTPlotSpaceDelegate>

@property (nonatomic) long populationForCalc;
@property (nonatomic) CGFloat yearForCalc;

@property (nonatomic) CGFloat disportExpenseEffect;

@property (nonatomic) CGFloat botoxExpenseEffect;

@property (nonatomic) CGFloat noneExpenseEffect;

@property (nonatomic, strong) UIPopoverController *popover;

@end

@implementation AZEController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.populationForCalc = 1;
    self.yearForCalc = 1.f;
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;

    self.disportExpenseEffect = app.disportOneYearExpenseEffect;
    self.botoxExpenseEffect = app.botoxOneYearExpenseEffect;
    self.noneExpenseEffect = app.noneOneYearExpenseEffect;
	// Do any additional setup after loading the view.

    [self updateUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateUI {
    [self generateData];
    [self generateLayout];
    self.chartView.hostedGraph = graph;
}

- (void)generateLayout
{
    //Create graph from theme
    graph                               = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    //[graph applyTheme:[CPTTheme themeNamed:kCPTPlainWhiteTheme]];
    
    graph.fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1.0f]];
    //self.hostedGraph                    = graph;
    graph.plotAreaFrame.masksToBorder   = NO;
    graph.paddingLeft                   = 0.0f;
    graph.paddingTop                    = 0.0f;
    graph.paddingRight                  = 0.0f;
    graph.paddingBottom                 = 0.0f;
    
    CPTMutableLineStyle *borderLineStyle    = [CPTMutableLineStyle lineStyle];
    borderLineStyle.lineColor               = [CPTColor whiteColor];
    borderLineStyle.lineWidth               = 0.0f;
    graph.plotAreaFrame.borderLineStyle     = borderLineStyle;
    graph.plotAreaFrame.paddingTop          = 10.0;
    graph.plotAreaFrame.paddingRight        = 10.0;
    graph.plotAreaFrame.paddingBottom       = 80.0;
    graph.plotAreaFrame.paddingLeft         = 70.0;
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;

    long long calc_max = (app.noneCostsToxinA+
                          app.noneCostsMedicalHelp+
                          app.noneCostsRelaxMedicalHelp+
                          app.noneCostsPension+
                          app.noneTwoYearsCostsOperations+
                          app.noneCostsPlastering+
                          app.noneCostsCompensation
                          +app.noneCostsAllowanceForKids)/self.noneExpenseEffect*1.1*self.yearForCalc;
    
    
    //Add plot space
    CPTXYPlotSpace *plotSpace       = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    plotSpace.delegate              = self;
    plotSpace.yRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)
                                                                   length:CPTDecimalFromLongLong(calc_max)];
    plotSpace.xRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(-1)
                                                                   length:CPTDecimalFromInt(4)];
    
    //Grid line styles
    CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
    majorGridLineStyle.lineWidth            = 0.75;
    majorGridLineStyle.lineColor            = [[CPTColor blackColor] colorWithAlphaComponent:0.1];
    CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
    minorGridLineStyle.lineWidth            = 0.0;
    minorGridLineStyle.lineColor            = [[CPTColor blackColor] colorWithAlphaComponent:0.1];
    
    //Axes
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    
    //X axis
    CPTXYAxis *x                    = axisSet.xAxis;
    x.orthogonalCoordinateDecimal   = CPTDecimalFromInt(0);
    x.majorIntervalLength           = CPTDecimalFromInt(1);
    x.minorTicksPerInterval         = 0;
    x.labelingPolicy                = CPTAxisLabelingPolicyNone;
    x.majorGridLineStyle            = majorGridLineStyle;
    x.axisConstraints               = [CPTConstraints constraintWithLowerOffset:0.0];
    
    //X labels
    int labelLocations = 0;
    NSMutableArray *customXLabels = [NSMutableArray array];
    for (NSString *day in preparats) {
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:day textStyle:x.labelTextStyle];
        newLabel.tickLocation   = [[NSNumber numberWithInt:labelLocations] decimalValue];
        newLabel.offset         = x.labelOffset + x.majorTickLength;
        //newLabel.rotation       = M_PI / 4;
        [customXLabels addObject:newLabel];
        labelLocations++;
    }
    //x.axisLabels                    = [NSSet setWithArray:customXLabels];
    
    //Y axis
    CPTXYAxis *y            = axisSet.yAxis;
    //y.title                 = @"Value";
    y.titleOffset           = 70.0f;
    
    //y.labelOffset           = -10.0f;
    y.labelingPolicy        = CPTAxisLabelingPolicyAutomatic;
    y.majorGridLineStyle    = majorGridLineStyle;
    y.minorGridLineStyle    = minorGridLineStyle;
    y.axisLineStyle         = minorGridLineStyle;
    y.axisConstraints       = [CPTConstraints constraintWithLowerOffset:0.0];
    
    CPTMutableTextStyle *style = [CPTMutableTextStyle textStyle];
    style.fontSize = 10.0f;
    y.labelTextStyle = style;
    
    //Create a bar line style
    CPTMutableLineStyle *barLineStyle   = [[CPTMutableLineStyle alloc] init];
    barLineStyle.lineWidth              = 0.0;
    barLineStyle.lineColor              = [CPTColor whiteColor];
    CPTMutableTextStyle *whiteTextStyle = [CPTMutableTextStyle textStyle];
    whiteTextStyle.color                = [CPTColor whiteColor];
    CPTMutableTextStyle *blackTextStyle = [CPTMutableTextStyle textStyle];
    blackTextStyle.color                = [CPTColor blackColor];
    
    //Plot
    BOOL firstPlot = YES;
    for (NSString *set in [[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
        //for (NSString *set in [sets allKeys]) {
        CPTBarPlot *plot        = [CPTBarPlot tubularBarPlotWithColor:[CPTColor blueColor] horizontalBars:NO];
        plot.lineStyle          = barLineStyle;
        plot.showLabels         = YES;
        plot.labelTextStyle     = style;
        plot.labelFormatter     = [ChartHelper currencyFormatter];
        
        CGColorRef color        = ((UIColor *)[sets objectForKey:set]).CGColor;
        plot.fill               = [CPTFill fillWithColor:[CPTColor colorWithCGColor:color]];
        if (firstPlot) {
            plot.barBasesVary   = NO;
            firstPlot           = NO;
        } else {
            plot.barBasesVary   = YES;
        }
        plot.barWidth           = CPTDecimalFromFloat(0.8f);
        plot.barsAreHorizontal  = NO;
        plot.dataSource         = self;
        plot.identifier         = set;
        plot.showLabels = YES;
        [graph addPlot:plot toPlotSpace:plotSpace];
    }
    
    /*
     //Add legend
     CPTLegend *theLegend      = [CPTLegend legendWithGraph:graph];
     theLegend.numberOfRows	  = sets.count;
     theLegend.fill			  = [CPTFill fillWithColor:[CPTColor colorWithCGColor:[UIColor clearColor].CGColor]];
     theLegend.borderLineStyle = barLineStyle;
     theLegend.cornerRadius	  = 0.0;
     theLegend.swatchSize	  = CGSizeMake(18.0, 18.0);
     whiteTextStyle.fontSize	  = 10.0;
     theLegend.textStyle		  = blackTextStyle;
     theLegend.rowMargin		  = 5.0;
     theLegend.paddingLeft	  = 10.0;
     theLegend.paddingTop	  = 10.0;
     theLegend.paddingRight	  = 10.0;
     theLegend.paddingBottom	  = 10.0;
     graph.legend              = theLegend;
     graph.legendAnchor        = CPTRectAnchorTopRight;
     graph.legendDisplacement  = CGPointMake(80.0, -10.0);
     */

}

- (void)generateData
{
    NSMutableDictionary *dataTemp = [[NSMutableDictionary alloc] init];
    
    
    //Array containing all the dates that will be displayed on the X axis
    preparats = [NSArray arrayWithObjects:@"Диспорт", @"Ботокс",
                 @"Стандартная терапия без БТА", nil];
    
    //Dictionary containing the name of the two sets and their associated color
    //used for the demo
    
    sets = [NSDictionary dictionaryWithObjectsAndKeys:
            RED_COLOR, @"3",
            BLUE_COLOR, @"2",
            GREEN_COLOR, @"1",
            nil];
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    CGFloat year_popul = self.yearForCalc*self.populationForCalc;
    
    
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    CGFloat costsOp = 0.0;
    switch (app.monthsForCalc) {
        case 4:
            costsOp = app.disportFourMouthCostsOperations;
            break;
        case 12:
            costsOp = app.disportOneYearCostsOperations;
            break;
        case 24:
            costsOp = app.disportTwoYearsCostsOperations;
            break;
            
        default:
            break;
    }
    [dict setObject:[NSNumber numberWithFloat:((float)(app.disportCostsToxinA+
                                                  app.disportCostsMedicalHelp+
                                                  app.disportCostsRelaxMedicalHelp+
                                                  app.disportCostsPension+
                                                  costsOp+
                                                  app.disportCostsPlastering+
                                                  app.disportCostsCompensation
                                                  +app.disportCostsAllowanceForKids)*year_popul)/self.disportExpenseEffect] forKey:@"1"];
    
    [dataTemp setObject:dict forKey:preparats[0]];
    
    dict = [NSMutableDictionary dictionary];
    switch (app.monthsForCalc) {
        case 4:
            costsOp = app.botoxFourMouthCostsOperations;
            break;
        case 12:
            costsOp = app.botoxOneYearCostsOperations;
            break;
        case 24:
            costsOp = app.botoxTwoYearsCostsOperations;
            break;
            
        default:
            break;
    }
    [dict setObject:[NSNumber numberWithFloat:((float)(app.botoxCostsToxinA+
                                                  app.botoxCostsMedicalHelp+
                                                  app.botoxCostsRelaxMedicalHelp+
                                                  app.botoxCostsPension+
                                                  costsOp+
                                                  app.botoxCostsPlastering+
                                                  app.botoxCostsCompensation
                                                  +app.botoxCostsAllowanceForKids)*year_popul)/self.botoxExpenseEffect] forKey:@"2"];
    [dataTemp setObject:dict forKey:preparats[1]];
    
    dict = [NSMutableDictionary dictionary];
    
    switch (app.monthsForCalc) {
        case 4:
            costsOp = app.noneFourMouthCostsOperations;
            break;
        case 12:
            costsOp = app.noneOneYearCostsOperations;
            break;
        case 24:
            costsOp = app.noneTwoYearsCostsOperations;
            break;
            
        default:
            break;
    }
    [dict setObject:[NSNumber numberWithFloat:((float)(app.noneCostsToxinA+
                                                  app.noneCostsMedicalHelp+
                                                  app.noneCostsRelaxMedicalHelp+
                                                  app.noneCostsPension+
                                                  costsOp+
                                                  app.noneCostsPlastering+
                                                  app.noneCostsCompensation
                                                  +app.noneCostsAllowanceForKids)*year_popul)/self.noneExpenseEffect] forKey:@"3"];
    [dataTemp setObject:dict forKey:preparats[2]];
    

    
    data = [dataTemp copy];
    
    NSLog(@"%@", data);
    
    
}

#pragma mark - CPTPlotDataSource methods

- (NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    return preparats.count;
}


-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)idx {
    
    NSNumber *num = [NSNumber numberWithFloat:NAN];
    
    if (fieldEnum == 0) {
        num = [NSNumber numberWithFloat:idx];
    }
    
    else {
        CGFloat offset = 0;
        if (((CPTBarPlot *)plot).barBasesVary) {
            for (NSString *set in [[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
                //for (NSString *set in [sets allKeys]) {
                if ([plot.identifier isEqual:set]) {
                    break;
                }
                offset += [[[data objectForKey:[preparats objectAtIndex:idx]] objectForKey:set] floatValue];
            }
        }
        
        //Y Value
        if (fieldEnum == 1) {
            num = [NSNumber numberWithFloat:[[[data objectForKey:[preparats objectAtIndex:idx]] objectForKey:plot.identifier] floatValue] + offset];
        }
        
        //Offset for stacked bar
        else {
            num = [NSNumber numberWithLongLong:offset];
        }
    }
    
    return num;

}

- (IBAction)changePeriod:(id)sender {
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        self.populationForCalc = 1;
        self.yearForCalc = 1.0f;
        
        self.disportExpenseEffect = app.disportOneYearExpenseEffect;
        self.botoxExpenseEffect = app.botoxOneYearExpenseEffect;
        self.noneExpenseEffect = app.noneOneYearExpenseEffect;
        

        
    }
    if (self.segmentedControl.selectedSegmentIndex == 1) {
        self.populationForCalc = 1;
        self.yearForCalc = 2.0f;
        
        self.disportExpenseEffect = app.disportTwoYearsExpenseEffect;
        self.botoxExpenseEffect = app.botoxTwoYearsExpenseEffect;
        self.noneExpenseEffect = app.noneTwoYearsExpenseEffect;
        

        
    }
    if (self.segmentedControl.selectedSegmentIndex == 2) {
        self.populationForCalc = 1;
        self.yearForCalc = 0.4f;
        
        self.disportExpenseEffect = app.disportFourMouthExpenseEffect;
        self.botoxExpenseEffect = app.botoxFourMouthExpenseEffect;
        self.noneExpenseEffect = app.noneFourMouthExpenseEffect;
        

    }
    

    
    [self updateUI];
    
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ShowFilter2"]) {
        UIStoryboardPopoverSegue *popoverSegue = (UIStoryboardPopoverSegue *)segue;
        popoverSegue.popoverController.backgroundColor = [UIColor whiteColor];
        self.popover = popoverSegue.popoverController;
    }
}


@end
