//
//  BDController.h
//  ipsen
//
//  Created by oles on 8/8/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BDController : UITableViewController

@property (weak, nonatomic) IBOutlet UILabel *disportVsBotoxSign3M;
@property (weak, nonatomic) IBOutlet UILabel *disportVsBotoxSign1Y;
@property (weak, nonatomic) IBOutlet UILabel *disportVsBotoxValue3M;
@property (weak, nonatomic) IBOutlet UILabel *disportVsBotoxValue1Y;

@property (weak, nonatomic) IBOutlet UILabel *disportVsXeominSign3M;
@property (weak, nonatomic) IBOutlet UILabel *disportVsXeominSign1Y;
@property (weak, nonatomic) IBOutlet UILabel *disportVsXeominValue3M;
@property (weak, nonatomic) IBOutlet UILabel *disportVsXeominValue1Y;

@property (weak, nonatomic) IBOutlet UILabel *disportVsNoneSign3M;
@property (weak, nonatomic) IBOutlet UILabel *disportVsNoneSign1Y;
@property (weak, nonatomic) IBOutlet UILabel *disportVsNoneValue3M;
@property (weak, nonatomic) IBOutlet UILabel *disportVsNoneValue1Y;


@end
