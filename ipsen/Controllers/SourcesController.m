//
//  SourcesController.m
//  ipsen
//
//  Created by oles on 7/17/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "SourcesController.h"
#import "LitCell.h"

@interface SourcesController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray *internal;
@property (nonatomic, strong) NSArray *external;
@property (nonatomic, strong) NSArray *web;

@end

@implementation SourcesController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"];
//    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:filePath];

    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSDictionary *models = [app.dict objectForKey:@"sources"];
    
    self.internal = [models objectForKey:@"internal"];
    self.external = [models objectForKey:@"external"];
    self.web = [models objectForKey:@"web"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (self.tabSwitcher.selectedSegmentIndex == 0) {
        return self.internal.count+2+self.external.count;
    } else {
        return self.web.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.tabSwitcher.selectedSegmentIndex == 0) {
        if (indexPath.row == 0) {
            return 30;
        } else if (indexPath.row == self.internal.count+1) {
            return 30;
        } else {
            return 50;
        }
    } else {
        return 50;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.tabSwitcher.selectedSegmentIndex == 0) {
        if (indexPath.row == 0) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Internal" forIndexPath:indexPath];
            return cell;
        } else if (indexPath.row == self.internal.count+1) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"External" forIndexPath:indexPath];
            return cell;
        } else {
            LitCell *cell;
            cell = (LitCell *)[tableView dequeueReusableCellWithIdentifier:@"LitCell" forIndexPath:indexPath];
            if (indexPath.row<self.internal.count+1) {
                cell.litTitle.text = self.internal[indexPath.row-1];
            } else {
                cell.litTitle.text = self.external[indexPath.row-2-self.internal.count];
            }
            return cell;
        }
    } else {
        LitCell *cell;
        cell = (LitCell *)[tableView dequeueReusableCellWithIdentifier:@"LitCell" forIndexPath:indexPath];
        cell.litTitle.text = self.web[indexPath.row];
        return cell;

    }
}

- (IBAction)tabSwitched:(id)sender {
    [self.tableView reloadData];
}
        
@end
