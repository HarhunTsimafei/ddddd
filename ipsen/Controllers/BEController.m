//
//  BEController.m
//  ipsen
//
//  Created by oles on 8/8/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "BEController.h"

@interface BEController ()<CPTPlotDataSource, CPTPlotSpaceDelegate>

@end

@implementation BEController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:NOTIFICATION_UPDATE_UI object:nil];
    
    //Array containing all the dates that will be displayed on the X axis
    preparats = [NSArray arrayWithObjects:@"Ботокс", @"Ксеомин",
                 @"Стандартная терапия без БТА", nil];
    
    //Dictionary containing the name of the two sets and their associated color
    //used for the demo
    sets = [NSDictionary dictionaryWithObjectsAndKeys:
            BOTOX_VS_DISPORT_COLOR, @"0.Ботокс",
            XEOMIN_VS_DISPORT_COLOR, @"1.Ксеомин",
            NONE_VS_DISPORT_COLOR, @"2.Без БТА",
            nil];
    
    
    [self refreshData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 2;
}

- (void)refreshData {
    [self generateData];
    [self generateLayout1];
    self.chartView3M.hostedGraph = graph1;
    [self generateLayout2];
    self.chartView1Y.hostedGraph = graph2;
}

- (void)generateLayout1
{
    //Create graph from theme
	graph1                               = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
	//[graph applyTheme:[CPTTheme themeNamed:kCPTPlainWhiteTheme]];
	
    graph1.fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1.0f]];
    //self.hostedGraph                    = graph;
    graph1.plotAreaFrame.masksToBorder   = NO;
    graph1.paddingLeft                   = 0.0f;
    graph1.paddingTop                    = 0.0f;
	graph1.paddingRight                  = 50.0f;
	graph1.paddingBottom                 = 0.0f;
    
    CPTMutableLineStyle *borderLineStyle    = [CPTMutableLineStyle lineStyle];
	borderLineStyle.lineColor               = [CPTColor whiteColor];
	borderLineStyle.lineWidth               = 0.0f;
	graph1.plotAreaFrame.borderLineStyle     = borderLineStyle;
	graph1.plotAreaFrame.paddingTop          = 10.0;
	graph1.plotAreaFrame.paddingRight        = 10.0;
	graph1.plotAreaFrame.paddingBottom       = 80.0;
	graph1.plotAreaFrame.paddingLeft         = 70.0;
    
	//Add plot space
	CPTXYPlotSpace *plotSpace       = (CPTXYPlotSpace *)graph1.defaultPlotSpace;
    plotSpace.delegate              = self;
	plotSpace.yRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(-50000)
                                                                   length:CPTDecimalFromLongLong(+100000)];
	plotSpace.xRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(-1)
                                                                   length:CPTDecimalFromInt(4)];
    
    //Grid line styles
	CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
	majorGridLineStyle.lineWidth            = 0.75;
	majorGridLineStyle.lineColor            = [[CPTColor blackColor] colorWithAlphaComponent:0.1];
    CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
	minorGridLineStyle.lineWidth            = 0.0;
	minorGridLineStyle.lineColor            = [[CPTColor blackColor] colorWithAlphaComponent:0.0];
    
    //Axes
	CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph1.axisSet;
    
    //X axis
    CPTXYAxis *x                    = axisSet.xAxis;
    x.orthogonalCoordinateDecimal   = CPTDecimalFromInt(0);
	x.majorIntervalLength           = CPTDecimalFromInt(1);
	x.minorTicksPerInterval         = 0;
    x.labelingPolicy                = CPTAxisLabelingPolicyNone;
    x.majorGridLineStyle            = majorGridLineStyle;
    x.axisConstraints               = [CPTConstraints constraintWithLowerOffset:0.0];
    
    //X labels
    /*
     int labelLocations = 0;
     NSMutableArray *customXLabels = [NSMutableArray array];
     for (NSString *day in preparats) {
     CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:day textStyle:x.labelTextStyle];
     newLabel.tickLocation   = [[NSNumber numberWithInt:labelLocations] decimalValue];
     newLabel.offset         = x.labelOffset + x.majorTickLength;
     //newLabel.rotation       = M_PI / 4;
     [customXLabels addObject:newLabel];
     labelLocations++;
     }
     //x.axisLabels                    = [NSSet setWithArray:customXLabels];
     */
    
    //Y axis
    ///*
	CPTXYAxis *y            = axisSet.yAxis;
	//y.title                 = @"Value";
	y.titleOffset           = 0.0f;
    
    y.labelOffset           = 0.0f;
    y.labelingPolicy        = CPTAxisLabelingPolicyAutomatic;
    y.majorGridLineStyle    = majorGridLineStyle;
    y.minorGridLineStyle    = minorGridLineStyle;
    y.axisLineStyle         = minorGridLineStyle;
    y.axisConstraints       = [CPTConstraints constraintWithLowerOffset:0.0];
    
    CPTMutableTextStyle *style = [CPTMutableTextStyle textStyle];
    style.fontSize = 10.0f;
    y.labelTextStyle = style;
    //*/
    
    //Create a bar line style
    CPTMutableLineStyle *barLineStyle   = [[CPTMutableLineStyle alloc] init];
    barLineStyle.lineWidth              = 0.0;
    barLineStyle.lineColor              = [CPTColor whiteColor];
    CPTMutableTextStyle *whiteTextStyle = [CPTMutableTextStyle textStyle];
	whiteTextStyle.color                = [CPTColor whiteColor];
    CPTMutableTextStyle *blackTextStyle = [CPTMutableTextStyle textStyle];
	whiteTextStyle.color                = [CPTColor blackColor];
    
    //Plot
    BOOL firstPlot = YES;
    for (NSString *set in [[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
        //for (NSString *set in [sets allKeys]) {
        CPTBarPlot *plot        = [CPTBarPlot tubularBarPlotWithColor:[CPTColor blueColor] horizontalBars:NO];
        plot.lineStyle          = barLineStyle;
        plot.showLabels         = YES;
        plot.labelTextStyle     = style;
        plot.labelFormatter     = [ChartHelper currencyFormatter];
        CGColorRef color        = ((UIColor *)[sets objectForKey:set]).CGColor;
        plot.fill               = [CPTFill fillWithColor:[CPTColor colorWithCGColor:color]];
        plot.barBasesVary       = NO;
        //        if (firstPlot) {
        //            plot.barBasesVary   = NO;
        //            firstPlot           = NO;
        //        } else {
        //            plot.barBasesVary   = YES;
        //        }
        plot.barWidth           = CPTDecimalFromFloat(0.7f);
        plot.barsAreHorizontal  = NO;
        plot.dataSource         = self;
        plot.identifier         = set;
        [graph1 addPlot:plot toPlotSpace:plotSpace];
    }
}

- (void)generateLayout2
{
    //Create graph from theme
	graph2                               = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
	//[graph applyTheme:[CPTTheme themeNamed:kCPTPlainWhiteTheme]];
	
    graph2.fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1.0f]];
    //self.hostedGraph                    = graph;
    graph2.plotAreaFrame.masksToBorder   = NO;
    graph2.paddingLeft                   = 0.0f;
    graph2.paddingTop                    = 0.0f;
	graph2.paddingRight                  = 50.0f;
	graph2.paddingBottom                 = 0.0f;
    
    CPTMutableLineStyle *borderLineStyle    = [CPTMutableLineStyle lineStyle];
	borderLineStyle.lineColor               = [CPTColor whiteColor];
	borderLineStyle.lineWidth               = 0.0f;
	graph2.plotAreaFrame.borderLineStyle     = borderLineStyle;
	graph2.plotAreaFrame.paddingTop          = 10.0;
	graph2.plotAreaFrame.paddingRight        = 10.0;
	graph2.plotAreaFrame.paddingBottom       = 80.0;
	graph2.plotAreaFrame.paddingLeft         = 70.0;
    
    //    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    //    long long calc_max =
    //    (app.disportGdpLossTotal+app.disportTotalPrice+app.disportDisPayouts+app.disportInvPayouts+app.disportSideTreatment+app.disportInsultTreatment)*1.2f;
    
	//Add plot space
	CPTXYPlotSpace *plotSpace       = (CPTXYPlotSpace *)graph2.defaultPlotSpace;
    plotSpace.delegate              = self;
	plotSpace.yRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(-100000)
                                                                   length:CPTDecimalFromLongLong(+200000)];
	plotSpace.xRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(-1)
                                                                   length:CPTDecimalFromInt(4)];
    
    //Grid line styles
	CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
	majorGridLineStyle.lineWidth            = 0.75;
	majorGridLineStyle.lineColor            = [[CPTColor blackColor] colorWithAlphaComponent:0.1];
    CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
	minorGridLineStyle.lineWidth            = 0.0;
	minorGridLineStyle.lineColor            = [[CPTColor blackColor] colorWithAlphaComponent:0.0];
    //minorGridLineStyle.miterLimit             = 0.0f;
    
    //Axes
	CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph2.axisSet;
    
    //X axis
    CPTXYAxis *x                    = axisSet.xAxis;
    x.orthogonalCoordinateDecimal   = CPTDecimalFromInt(0);
	x.majorIntervalLength           = CPTDecimalFromInt(1);
	x.minorTicksPerInterval         = 0;
    x.labelingPolicy                = CPTAxisLabelingPolicyNone;
    x.majorGridLineStyle            = majorGridLineStyle;
    x.axisConstraints               = [CPTConstraints constraintWithLowerOffset:0.0];
    x.axisLineStyle                 = majorGridLineStyle;
    
    //X labels
    /*
     int labelLocations = 0;
     NSMutableArray *customXLabels = [NSMutableArray array];
     for (NSString *day in preparats) {
     CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:day textStyle:x.labelTextStyle];
     newLabel.tickLocation   = [[NSNumber numberWithInt:labelLocations] decimalValue];
     newLabel.offset         = x.labelOffset + x.majorTickLength;
     //newLabel.rotation       = M_PI / 4;
     [customXLabels addObject:newLabel];
     labelLocations++;
     }
     //x.axisLabels                    = [NSSet setWithArray:customXLabels];
     */
    
    //Y axis
    ///*
	CPTXYAxis *y            = axisSet.yAxis;
	//y.title                 = @"Value";
	y.titleOffset           = 0.0f;
    
    y.labelOffset           = 0.0f;
    y.labelingPolicy        = CPTAxisLabelingPolicyAutomatic;
    y.majorGridLineStyle    = majorGridLineStyle;
    y.minorGridLineStyle    = nil;
    y.axisLineStyle         = minorGridLineStyle;
    y.axisConstraints       = [CPTConstraints constraintWithLowerOffset:0.0];
    
    CPTMutableTextStyle *style = [CPTMutableTextStyle textStyle];
    style.fontSize = 10.0f;
    y.labelTextStyle = style;
    //*/
    
    //Create a bar line style
    CPTMutableLineStyle *barLineStyle   = [[CPTMutableLineStyle alloc] init];
    barLineStyle.lineWidth              = 0.0;
    barLineStyle.lineColor              = [CPTColor whiteColor];
    CPTMutableTextStyle *whiteTextStyle = [CPTMutableTextStyle textStyle];
	whiteTextStyle.color                = [CPTColor whiteColor];
    CPTMutableTextStyle *blackTextStyle = [CPTMutableTextStyle textStyle];
	whiteTextStyle.color                = [CPTColor blackColor];
    
    //Plot
    BOOL firstPlot = YES;
    for (NSString *set in [[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
        //for (NSString *set in [sets allKeys]) {
        CPTBarPlot *plot        = [CPTBarPlot tubularBarPlotWithColor:[CPTColor blueColor] horizontalBars:NO];
        plot.lineStyle          = barLineStyle;
        plot.showLabels         = YES;
        plot.labelTextStyle     = style;
        plot.labelFormatter     = [ChartHelper currencyFormatter];
        CGColorRef color        = ((UIColor *)[sets objectForKey:set]).CGColor;
        plot.fill               = [CPTFill fillWithColor:[CPTColor colorWithCGColor:color]];
        //        if (firstPlot) {
        //            plot.barBasesVary   = NO;
        //            firstPlot           = NO;
        //        } else {
        //            plot.barBasesVary   = YES;
        //        }
        plot.barWidth           = CPTDecimalFromFloat(0.7f);
        plot.barsAreHorizontal  = NO;
        plot.dataSource         = self;
        plot.identifier         = set;
        [graph2 addPlot:plot toPlotSpace:plotSpace];
    }
}

- (void) generateData {
    NSMutableDictionary *dataTemp = [[NSMutableDictionary alloc] init];
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    // calculations
    // disport
    long long disport_total_1_3M =
    (((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*5*(1-0.17)))/4)
    +
    ((app.priceDisport*2)*(3/3))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*(1-0.17))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*3*(1-0.17f))
    +
    (16538/5/((0)?1:4)*((1)?1:(0?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((1)?1:((app.qtySick3M))));
    
    long long disport_total_1_1Y =
    ((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*(1-0.17)))
    +
    ((app.priceDisport*2)*(12/3))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*5*(1-0.17))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*12*(1-0.17f))
    +
    (16538/5/((1)?1:4)*(1))
    +
    ((177793+32287)*(1));
    
    // botox
    long long botox_total_1_3M =
    (((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*5*(1-0.127)))/4)
    +
    ((app.priceBotox*3)*(3/3))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*(1-0.127))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*3*(1-0.127f))
    +
    (22250/5/((0)?1:4)*((1)?1:(app.qtySick3M)))
    +
    ((177793+32287)*((1)?1:((app.qtySick3M))));
    
    long long botox_total_1_1Y =
    ((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*(1-0.127)))
    +
    ((app.priceBotox*3)*(12/3))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*5*(1-0.127))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*12*(1-0.127f))
    +
    (22250/5/((1)?1:4)*((1)?1:(1?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((1)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    
    // xeomin
    long long xeomin_total_1_3M =
    (((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*5*(1-0.1233f)))/4)
    +
    ((app.priceXeomin*4)*(3/3))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*(1-0.1233f))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*3*(1-0.1233f))
    +
    (24029/5/((0)?1:4)*((1)?1:(0?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((1)?1:((0?app.qtySick1Y:app.qtySick3M))));
    
    long long xeomin_total_1_1Y =
    ((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*(1-0.1233f)))
    +
    ((app.priceXeomin*4)*(12/3))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*5*(1-0.1233f))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*12*(1-0.1233f))
    +
    (24029/5/((1)?1:4)*((1)?1:(1?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((1)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    // none
    long long none_total_1_3M =
    (((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*5*(1-0.0333f)))/4)
    +
    0
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*(1-0.0333f))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*3*(1-0.0333f))
    +
    0
    +
    ((177793+32287)*((1)?1:((0?app.qtySick1Y:app.qtySick3M))));
    
    long long none_total_1_1Y =
    ((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*(1-0.0333f)))
    +
    0
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*5*(1-0.0333f))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*1*12*(1-0.0333f))
    +
    0
    +
    ((177793+32287)*((1)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    self.botoxVSdisport3M.text = MONEY_NUMBER(disport_total_1_3M-botox_total_1_3M);
    self.xeominVSdisport3M.text = MONEY_NUMBER(disport_total_1_3M-xeomin_total_1_3M);
    self.noneVSdisport3M.text = MONEY_NUMBER(disport_total_1_3M-none_total_1_3M);
    
    self.botoxVSdisport1Y.text = MONEY_NUMBER(disport_total_1_1Y-botox_total_1_1Y);
    self.xeominVSdisport1Y.text = MONEY_NUMBER(disport_total_1_1Y-xeomin_total_1_1Y);
    self.noneVSdisport1Y.text = MONEY_NUMBER(disport_total_1_1Y-none_total_1_1Y);
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSNumber numberWithLongLong:disport_total_1_3M-botox_total_1_3M] forKey:[[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)][0]];
    [dataTemp setObject:dict forKey:preparats[0]];
    
    dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSNumber numberWithLongLong:disport_total_1_3M-xeomin_total_1_3M] forKey:[[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)][1]];
    [dataTemp setObject:dict forKey:preparats[1]];
    
    dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSNumber numberWithLongLong:disport_total_1_3M-none_total_1_3M] forKey:[[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)][2]];
    [dataTemp setObject:dict forKey:preparats[2]];
    
    data1 = [dataTemp copy];
    
    NSMutableDictionary *dataTemp2 = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary *dict2 = [NSMutableDictionary dictionary];
    [dict2 setObject:[NSNumber numberWithLongLong:disport_total_1_1Y-botox_total_1_1Y] forKey:[[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)][0]];
    [dataTemp2 setObject:dict2 forKey:preparats[0]];
    
    dict2 = [NSMutableDictionary dictionary];
    [dict2 setObject:[NSNumber numberWithLongLong:disport_total_1_1Y-xeomin_total_1_1Y] forKey:[[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)][1]];
    [dataTemp2 setObject:dict2 forKey:preparats[1]];
    
    dict2 = [NSMutableDictionary dictionary];
    [dict2 setObject:[NSNumber numberWithLongLong:disport_total_1_1Y-none_total_1_1Y] forKey:[[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)][2]];
    [dataTemp2 setObject:dict2 forKey:preparats[2]];
    
    data2 = [dataTemp2 copy];
    
}

#pragma mark - CPTPlotDataSource methods

- (NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    return preparats.count;
}

- (CPTFill *)barFillForBarPlot:(CPTBarPlot *)barPlot recordIndex:(NSUInteger)index
{
    CPTFill *fill = [CPTFill fillWithColor:[sets objectForKey:[[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)][index]]];
    return fill;
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)idx {
    NSNumber *num = [NSNumber numberWithLongLong:NAN];
    
    if (fieldEnum == 0) {
        num = [NSNumber numberWithLongLong:idx];
    }
    
    else {
        long long offset = 0;
        if (((CPTBarPlot *)plot).barBasesVary) {
            //            for (NSString *set in [[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
            for (NSString *set in [[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
                if ([plot.identifier isEqual:set]) {
                    break;
                }
                if (plot.graph == graph1) {
                    offset += [[[data1 objectForKey:[preparats objectAtIndex:idx]] objectForKey:set] longLongValue];
                } else {
                    offset += [[[data2 objectForKey:[preparats objectAtIndex:idx]] objectForKey:set] longLongValue];
                }
            }
        }
        
        //Y Value
        if (fieldEnum == 1) {
            if (plot.graph == graph1) {
                num = [NSNumber numberWithLongLong:[[[data1 objectForKey:[preparats objectAtIndex:idx]] objectForKey:plot.identifier] longLongValue] + offset];
            } else {
                num = [NSNumber numberWithLongLong:[[[data2 objectForKey:[preparats objectAtIndex:idx]] objectForKey:plot.identifier] longLongValue] + offset];
            }
        }
        
    }
    
    return num;
}

@end
