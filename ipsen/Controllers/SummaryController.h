//
//  SummaryController.h
//  ipsen
//
//  Created by oles on 7/17/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SummaryController : UITableViewController

- (IBAction)showLevel:(id)sender;
- (IBAction)showCoeff:(id)sender;


@end
