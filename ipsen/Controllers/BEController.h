//
//  BEController.h
//  ipsen
//
//  Created by oles on 8/8/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BEController : UITableViewController {

    NSDictionary *sets;
    NSDictionary *sets2;
    NSArray *preparats;
    
    CPTXYGraph *graph1;
    NSDictionary *data1;
    
    CPTXYGraph *graph2;
    NSDictionary *data2;

}

@property (weak, nonatomic) IBOutlet CPTGraphHostingView *chartView3M;
@property (weak, nonatomic) IBOutlet CPTGraphHostingView *chartView1Y;
@property (weak, nonatomic) IBOutlet UILabel *botoxVSdisport3M;
@property (weak, nonatomic) IBOutlet UILabel *xeominVSdisport3M;
@property (weak, nonatomic) IBOutlet UILabel *noneVSdisport3M;

@property (weak, nonatomic) IBOutlet UILabel *botoxVSdisport1Y;
@property (weak, nonatomic) IBOutlet UILabel *xeominVSdisport1Y;
@property (weak, nonatomic) IBOutlet UILabel *noneVSdisport1Y;

@end
