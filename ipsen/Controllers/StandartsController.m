//
//  StandartsController.m
//  ipsen
//
//  Created by oles on 7/17/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "StandartsController.h"
#import "CodeCell.h"

@interface StandartsController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSDictionary *model;

@property (nonatomic) int currentSick;

@end

@implementation StandartsController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.currentSick = 0;
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData {
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"];
//    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:filePath];
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    NSArray *models = [app.dict objectForKey:@"sicks"];
    self.model = models[self.currentSick];
    
    self.modelCodeLabel.text = [self.model objectForKey:@"field0"];
    NSNumber *cost = [self.model objectForKey:@"cost"];
    self.illnessCost.text = MONEY_NUMBER(cost.longLongValue);
    self.field1Label.text = [self.model objectForKey:@"field1"];
    self.field2Label.text = [self.model objectForKey:@"field2"];
    self.field3Label.text = [self.model objectForKey:@"field3"];
    self.field4Label.text = [self.model objectForKey:@"field4"];
    self.field5Label.text = [self.model objectForKey:@"field5"];
    self.field6Label.text = [self.model objectForKey:@"field6"];
    self.field7Label.text = [self.model objectForKey:@"field7"];
    self.field8Label.text = [self.model objectForKey:@"field8"];
    self.field9Label.text = [self.model objectForKey:@"field9"];
    //self.field10Label.text = [self.model objectForKey:@"field10"];
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSArray *illnesses = [self.model objectForKey:@"illnesses"];
    NSArray *services = [self.model objectForKey:@"services"];
    NSArray *drugs = [self.model objectForKey:@"drugs"];
    
    if (self.tabSwitcher.selectedSegmentIndex == 0) {
        return illnesses.count;
    } else if (self.tabSwitcher.selectedSegmentIndex == 1) {
        return services.count;
    } else {
        return drugs.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CodeCell";
    CodeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    NSArray *illnesses = [self.model objectForKey:@"illnesses"];
    NSArray *services = [self.model objectForKey:@"services"];
    NSArray *drugs = [self.model objectForKey:@"drugs"];
    NSString *code;
    NSString *name;
    
    if (self.tabSwitcher.selectedSegmentIndex == 0) {
        NSDictionary *illness = illnesses[indexPath.row];
        code = [illness objectForKey:@"code"];
        name = [illness objectForKey:@"name"];
    } else if (self.tabSwitcher.selectedSegmentIndex == 1) {
        NSDictionary *service = services[indexPath.row];
        code = [service objectForKey:@"code"];
        name = [service objectForKey:@"name"];
    } else {
        NSDictionary *drug = drugs[indexPath.row];
        code = [drug objectForKey:@"code"];
        name = [drug objectForKey:@"name"];
    }
    
    cell.nameLabel.text = name;
    cell.codeLabel.text = code;
    
    return cell;
}

- (IBAction)selectInsultAfter12:(id)sender {
    self.currentSick = 0;
    //[self.tableView reloadData];
    [self loadData];
    
    self.afterButton.selected = YES;
    self.insultButton.selected = NO;
    self.dermButton.selected = NO;
    self.bussitButton.selected = NO;
    self.deprButton.selected = NO;
    self.headButton.selected = NO;
}

- (IBAction)selectInsultBefore12:(id)sender {
    self.currentSick = 1;
    //[self.tableView reloadData];
    [self loadData];

    self.afterButton.selected = NO;
    self.insultButton.selected = YES;
    self.dermButton.selected = NO;
    self.bussitButton.selected = NO;
    self.deprButton.selected = NO;
    self.headButton.selected = NO;
}

- (IBAction)selectDerma:(id)sender {
    self.currentSick = 2;
    //[self.tableView reloadData];
    [self loadData];

    self.afterButton.selected = NO;
    self.insultButton.selected = NO;
    self.dermButton.selected = YES;
    self.bussitButton.selected = NO;
    self.deprButton.selected = NO;
    self.headButton.selected = NO;
}

- (IBAction)selectBursit:(id)sender {
    self.currentSick = 3;
    //[self.tableView reloadData];
    [self loadData];

    self.afterButton.selected = NO;
    self.insultButton.selected = NO;
    self.dermButton.selected = NO;
    self.bussitButton.selected = YES;
    self.deprButton.selected = NO;
    self.headButton.selected = NO;
    
}

- (IBAction)selectDepression:(id)sender {
    self.currentSick = 4;
    //[self.tableView reloadData];
    [self loadData];

    self.afterButton.selected = NO;
    self.insultButton.selected = NO;
    self.dermButton.selected = NO;
    self.bussitButton.selected = NO;
    self.deprButton.selected = YES;
    self.headButton.selected = NO;
}

- (IBAction)selectHeadake:(id)sender {
    self.currentSick = 5;
    //[self.tableView reloadData];
    [self loadData];

    self.afterButton.selected = NO;
    self.insultButton.selected = NO;
    self.dermButton.selected = NO;
    self.bussitButton.selected = NO;
    self.deprButton.selected = NO;
    self.headButton.selected = YES;
}

- (IBAction)tabSwitched:(id)sender {
    [self.tableView reloadData];
    switch (self.tabSwitcher.selectedSegmentIndex) {
        case 0:
        {
            self.header1Label.text = @"Код по МКБ Х";
            self.header2Label.text = @"Назологические единицы";
        }
            break;
        case 1:
        {
            self.header1Label.text = @"Код";
            self.header2Label.text = @"Услуги";
        }
            break;
        case 2:
        {
            self.header1Label.text = @"Код";
            self.header2Label.text = @"МНН";
        }
            break;
            
        default:
            break;
    }
}

@end
