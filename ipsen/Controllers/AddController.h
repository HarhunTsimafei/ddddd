//
//  AddController.h
//  ipsen
//
//  Created by oles on 8/4/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *standartsButton;
@property (weak, nonatomic) IBOutlet UIButton *sideButton;
@property (weak, nonatomic) IBOutlet UIButton *litButton;
@property (weak, nonatomic) IBOutlet UIView *titleView;

- (IBAction)selectStandarts:(id)sender;
- (IBAction)selectSide:(id)sender;
- (IBAction)selectLit:(id)sender;

- (IBAction)selectInsult:(id)sender;
- (IBAction)selectInsultAfter:(id)sender;
- (IBAction)selectDerm:(id)sender;
- (IBAction)selectBurs:(id)sender;
- (IBAction)selectDepr:(id)sender;
- (IBAction)selectHead:(id)sender;

@end
