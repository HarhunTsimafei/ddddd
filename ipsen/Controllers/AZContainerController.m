//
//  AZContainerController.m
//  ipsen
//
//  Created by oles on 7/16/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "AZContainerController.h"

@interface AZContainerController ()<UIPageViewControllerDelegate, UIPageViewControllerDataSource>

@property (weak, nonatomic) IBOutlet UISwitch *switchUI;


@property (strong, nonatomic) UIPageViewController *pageController;
@property (strong, nonatomic) AZTableController *tableController;
@property (strong, nonatomic) AZChartController *chartController;

@property (nonatomic, strong) UIPopoverController *popover;

@end

@implementation AZContainerController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	// Do any additional setup after loading the view.
    [self setupPageController];
    [self valueChanged:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataChanged) name:NOTIFICATION_DATA_CHANGED object:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)valueChanged:(id)sender{
    NSArray* arr = self.pageController.viewControllers;
    for (AZTableController *vc in arr) {
        vc.switchOn = self.switchUI.on;
        [vc updateUI];
    }
    for (AZChartController *vc in arr) {
        vc.switchOn = self.switchUI.on;
        [vc updateUI];
    }
}

- (void)dataChanged {
    [self.popover dismissPopoverAnimated:YES];
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    CGFloat month;
    if(app.monthsForCalc == 4){
        [self.filterButton setTitle:[NSString stringWithFormat:@"Затраты на %@ на %@",
                                  (app.populationForCalc == 1)?@"одного человека":@"всю популяцию",
                                  @"4 месяца"]
                           forState:UIControlStateNormal];
        month = 1/3.f;
    }
    if(app.monthsForCalc == 12){
        [self.filterButton setTitle:[NSString stringWithFormat:@"Затраты на %@ на %@",
                                     (app.populationForCalc == 1)?@"одного человека":@"всю популяцию",
                                     @"1 год"]
                           forState:UIControlStateNormal];
        month = 1;
    }
    if(app.monthsForCalc == 24){
        [self.filterButton setTitle:[NSString stringWithFormat:@"Затраты на %@ на %@",
                                     (app.populationForCalc == 1)?@"одного человека":@"всю популяцию",
                                     @"2 года"]
                           forState:UIControlStateNormal];
         month = 2;
    }
    NSArray* arr = self.pageController.viewControllers;
    for (AZTableController *vc in arr) {
        app.yearForCalc = month;
        NSArray* regions = [app.dict objectForKey:@"regions"];
        NSDictionary *region = regions[0];
        app.child_sick = app.sickInAllRegions;
        app.populationForCalcApp = (app.populationForCalc == 1)?1:app.child_sick;
        [vc updateUI];
    }
    for (AZChartController *vc in arr) {
        app.yearForCalc = month;
        NSArray* regions = [app.dict objectForKey:@"regions"];
        NSDictionary *region = regions[0];
        
        app.child_sick = app.sickInAllRegions;
        app.populationForCalcApp = (app.populationForCalc == 1)?1:app.child_sick;
        [vc updateUI];
    }
          
}

#pragma marks - Controllers

- (AZTableController *)tableController {
    if (!_tableController) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _tableController = (AZTableController *)[storyBoard instantiateViewControllerWithIdentifier:@"AZTable"];
    }
    return _tableController;
}

- (AZChartController *)chartController {
    if (!_chartController) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _chartController = (AZChartController *)[storyBoard instantiateViewControllerWithIdentifier:@"AZChart"];
    }
    return _chartController;
}

- (void)setupPageController {
    NSArray *viewControllers = @[self.chartController];
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.pageController.delegate = self;
    self.pageController.dataSource = self;
    self.pageController.view.backgroundColor = [UIColor clearColor];
    // disable swipe
    for (UIScrollView *view in self.pageController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            view.scrollEnabled = NO;
        }
    }
    
    [self addChildViewController:self.pageController];
    [self.containerView addSubview:self.pageController.view];
    
    // Set the page view controller's bounds using an inset rect so that self's view is visible around the edges of the pages.
    CGRect pageViewRect = self.containerView.bounds;
    pageViewRect = CGRectMake(pageViewRect.origin.x, pageViewRect.origin.y, pageViewRect.size.width, pageViewRect.size.height);
    self.pageController.view.frame = pageViewRect;
    
    [self.pageController didMoveToParentViewController:self];
    
    // Add the page view controller's gesture recognizers to the book view controller's view so that the gestures are started more easily.
    self.view.gestureRecognizers = self.pageController.gestureRecognizers;
}

- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    UIViewController *currentViewController = self.pageController.viewControllers[0];
    NSArray *viewControllers = @[currentViewController];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    
    self.pageController.doubleSided = NO;
    return UIPageViewControllerSpineLocationMin;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    return self.chartController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    return self.chartController;
}

- (IBAction)switchView:(id)sender {
    if (self.viewSwitcher.selectedSegmentIndex == 0) {
        NSArray *array = @[self.chartController];
        [self.pageController setViewControllers:array direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
    } else {
        NSArray *array = @[self.tableController];
        [self.pageController setViewControllers:array direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    }
}

- (IBAction)showFilter:(id)sender {
    //[self performSegueWithIdentifier:@"ShowFilter" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ShowFilter"]) {
        UIStoryboardPopoverSegue *popoverSegue = (UIStoryboardPopoverSegue *)segue;
        popoverSegue.popoverController.backgroundColor = [UIColor whiteColor];
        self.popover = popoverSegue.popoverController;
    }
}

@end
