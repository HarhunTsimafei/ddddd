//
//  BCController.h
//  ipsen
//
//  Created by oles on 8/8/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCController : UIViewController {
    CPTXYGraph *graph1;
	NSMutableArray *pieData1;
    CPTXYGraph * graph2;
	NSMutableArray *pieData2;
    NSDictionary *sets;
}

@property (weak, nonatomic) IBOutlet UISlider *curDisportSlider;
@property (weak, nonatomic) IBOutlet UISlider *curBotoxSlider;
@property (weak, nonatomic) IBOutlet UISlider *curXeominSlider;
@property (weak, nonatomic) IBOutlet UISlider *curNoneSlider;

@property (weak, nonatomic) IBOutlet UILabel *curDisportPercent;
@property (weak, nonatomic) IBOutlet UILabel *curBotoxPercent;
@property (weak, nonatomic) IBOutlet UILabel *curXeominPercent;
@property (weak, nonatomic) IBOutlet UILabel *curNonePercent;
@property (weak, nonatomic) IBOutlet UILabel *curTotalValue;
@property (weak, nonatomic) IBOutlet UILabel *curParts;


@property (weak, nonatomic) IBOutlet UISlider *modDisportSlider;
@property (weak, nonatomic) IBOutlet UISlider *modBotoxSlider;
@property (weak, nonatomic) IBOutlet UISlider *modXeominSlider;
@property (weak, nonatomic) IBOutlet UISlider *modNoneSlider;

@property (weak, nonatomic) IBOutlet UILabel *modDisportPercent;
@property (weak, nonatomic) IBOutlet UILabel *modBotoxPercent;
@property (weak, nonatomic) IBOutlet UILabel *modXeominPercent;
@property (weak, nonatomic) IBOutlet UILabel *modNonePercent;
@property (weak, nonatomic) IBOutlet UILabel *modTotalValue;

@property (weak, nonatomic) IBOutlet UILabel *modParts;

- (IBAction)curChanged:(id)sender;
- (IBAction)modChanged:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property (weak, nonatomic) IBOutlet CPTGraphHostingView *chartView1;
@property (weak, nonatomic) IBOutlet CPTGraphHostingView *chartView2;

@end
