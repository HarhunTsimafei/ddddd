//
//  MainController.h
//  ipsen
//
//  Created by oles on 7/15/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *sideView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *titleView;
@property (weak, nonatomic) IBOutlet UIButton *buttonData;
@property (weak, nonatomic) IBOutlet UIButton *buttonAnalysis;
@property (weak, nonatomic) IBOutlet UIButton *buttonAdd;
@property (weak, nonatomic) IBOutlet UIButton *buttonSummary;

- (IBAction)doAnalysis:(id)sender;
- (IBAction)doStandarts:(id)sender;
//- (IBAction)doSideEffects:(id)sender;
- (IBAction)doSummary:(id)sender;
- (IBAction)doData:(id)sender;
//- (IBAction)doSources:(id)sender;
- (IBAction)selectAZ:(id)sender;
- (IBAction)selectSens:(id)sender;
- (IBAction)selectEff:(id)sender;
- (IBAction)selectExpEff:(id)sender;
- (IBAction)selectBudget:(id)sender;

@end
