//
//  AZTableController.h
//  ipsen
//
//  Created by oles on 7/16/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AZTableController : UITableViewController

@property (assign, nonatomic) BOOL switchOn;

@property (nonatomic) long populationForCalc;
@property (assign, nonatomic) CGFloat yearForCalc;

- (void)updateUI;

@property (weak, nonatomic) IBOutlet UILabel *disportCostsToxinA;
@property (weak, nonatomic) IBOutlet UILabel *disportCostsMedicalHelp;
@property (weak, nonatomic) IBOutlet UILabel *disportCostsRelaxMedicalHelp;
@property (weak, nonatomic) IBOutlet UILabel *disportCostsPension;
@property (weak, nonatomic) IBOutlet UILabel *disportCostsOperations;
@property (weak, nonatomic) IBOutlet UILabel *disportCostsPlastering;
@property (weak, nonatomic) IBOutlet UILabel *disportCostsCompensation;
@property (weak, nonatomic) IBOutlet UILabel *disportCostsAllowanceForKids;

@property (weak, nonatomic) IBOutlet UILabel *botoxCostsToxinA;
@property (weak, nonatomic) IBOutlet UILabel *botoxCostsMedicalHelp;
@property (weak, nonatomic) IBOutlet UILabel *botoxCostsRelaxMedicalHelp;
@property (weak, nonatomic) IBOutlet UILabel *botoxCostsPension;
@property (weak, nonatomic) IBOutlet UILabel *botoxCostsOperations;
@property (weak, nonatomic) IBOutlet UILabel *botoxCostsPlastering;
@property (weak, nonatomic) IBOutlet UILabel *botoxCostsCompensation;
@property (weak, nonatomic) IBOutlet UILabel *botoxCostsAllowanceForKids;

@property (weak, nonatomic) IBOutlet UILabel *noneCostsToxinA;
@property (weak, nonatomic) IBOutlet UILabel *noneCostsMedicalHelp;
@property (weak, nonatomic) IBOutlet UILabel *noneCostsRelaxMedicalHelp;
@property (weak, nonatomic) IBOutlet UILabel *noneCostsPension;
@property (weak, nonatomic) IBOutlet UILabel *noneCostsOperations;
@property (weak, nonatomic) IBOutlet UILabel *noneCostsPlastering;
@property (weak, nonatomic) IBOutlet UILabel *noneCostsCompensation;
@property (weak, nonatomic) IBOutlet UILabel *noneCostsAllowanceForKids;

@property (weak, nonatomic) IBOutlet UILabel *disportCostsSum;
@property (weak, nonatomic) IBOutlet UILabel *botoxCostsSum;
@property (weak, nonatomic) IBOutlet UILabel *noneCostsSum;

@end
