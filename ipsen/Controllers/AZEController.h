//
//  AZEController.h
//  ipsen
//
//  Created by oles on 7/17/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AZEController : UIViewController
{
    NSDictionary *sets;
    NSArray *preparats;
    
    CPTXYGraph *graph;
    NSDictionary *data;


}

@property (weak, nonatomic) IBOutlet CPTGraphHostingView *chartView;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

- (IBAction)changePeriod:(id)sender;

@end
