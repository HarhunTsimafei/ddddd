//
//  StandartsController.h
//  ipsen
//
//  Created by oles on 7/17/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StandartsController : UIViewController

- (IBAction)selectInsultAfter12:(id)sender;
- (IBAction)selectInsultBefore12:(id)sender;
- (IBAction)selectDerma:(id)sender;
- (IBAction)selectBursit:(id)sender;
- (IBAction)selectDepression:(id)sender;
- (IBAction)selectHeadake:(id)sender;
- (IBAction)tabSwitched:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *afterButton;
@property (weak, nonatomic) IBOutlet UIButton *insultButton;
@property (weak, nonatomic) IBOutlet UIButton *dermButton;
@property (weak, nonatomic) IBOutlet UIButton *bussitButton;
@property (weak, nonatomic) IBOutlet UIButton *deprButton;
@property (weak, nonatomic) IBOutlet UIButton *headButton;

@property (nonatomic) int selectedItem;

@property (weak, nonatomic) IBOutlet UISegmentedControl *tabSwitcher;

@property (weak, nonatomic) IBOutlet UILabel *field1Label;
@property (weak, nonatomic) IBOutlet UILabel *field2Label;
@property (weak, nonatomic) IBOutlet UILabel *field3Label;
@property (weak, nonatomic) IBOutlet UILabel *field4Label;
@property (weak, nonatomic) IBOutlet UILabel *field5Label;
@property (weak, nonatomic) IBOutlet UILabel *field6Label;
@property (weak, nonatomic) IBOutlet UILabel *field7Label;
@property (weak, nonatomic) IBOutlet UILabel *field8Label;
@property (weak, nonatomic) IBOutlet UILabel *field9Label;
@property (weak, nonatomic) IBOutlet UILabel *field10Label;
@property (weak, nonatomic) IBOutlet UILabel *illnessCost;
@property (weak, nonatomic) IBOutlet UILabel *modelCodeLabel;

@property (weak, nonatomic) IBOutlet UILabel *header1Label;
@property (weak, nonatomic) IBOutlet UILabel *header2Label;


@end
