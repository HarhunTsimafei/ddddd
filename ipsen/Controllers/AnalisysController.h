//
//  AnalisysController.h
//  ipsen
//
//  Created by oles on 7/15/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnalisysController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *containerView;

- (IBAction)doAZ:(id)sender;
- (IBAction)doAE:(id)sender;
- (IBAction)doAZE:(id)sender;
- (IBAction)doBudget:(id)sender;
- (IBAction)doAS:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *titleView;

@property (weak, nonatomic) IBOutlet UIButton *buttonAZ;
@property (weak, nonatomic) IBOutlet UIButton *buttonEff;
@property (weak, nonatomic) IBOutlet UIButton *buttonExpEff;
@property (weak, nonatomic) IBOutlet UIButton *buttonBudget;
@property (weak, nonatomic) IBOutlet UIButton *buttonSens;

@property (nonatomic) int currentAnalysis;

@end
