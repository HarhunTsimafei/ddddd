//
//  SourcesController.h
//  ipsen
//
//  Created by oles on 7/17/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SourcesController : UIViewController

@property (weak, nonatomic) IBOutlet UISegmentedControl *tabSwitcher;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)tabSwitched:(id)sender;

@end
