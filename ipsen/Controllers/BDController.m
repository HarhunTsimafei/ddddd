//
//  BDController.m
//  ipsen
//
//  Created by oles on 8/8/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "BDController.h"

@interface BDController ()

@property (nonatomic) long long disport_total_ALL_1Y;
@property (nonatomic) long long botox_total_ALL_1Y;
@property (nonatomic) long long xeomin_total_ALL_1Y;
@property (nonatomic) long long none_total_ALL_1Y;

@end

@implementation BDController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:NOTIFICATION_UPDATE_UI object:nil];
    [self refreshData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 12;
}

- (void)refreshData {
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    // calculations
    // disport
    long long disport_total_ALL_3M =
//    ((((app.qtySick3M*(app.sickPercent1Group/100))+(app.qtySick3M*(app.sickPercent2Group/100))))*app.gdpPerHabitant/4)
//    +
    ((app.priceDisport*2)*(3/3)*(app.qtySick3M))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*app.qtySick3M*(1-0.17))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick3M*3*(1-0.17f))
    +
    (16538/5/(4)*((app.qtySick3M)))
    +
    ((177793+32287)*((0)?1:((app.qtySick3M))));
    
    long long disport_total_ALL_1Y =
//    ((((app.qtySick1Y*(app.sickPercent1Group/100))+(app.qtySick1Y*(app.sickPercent2Group/100)))*app.gdpPerHabitant*(1-0.17)))
//    +
    ((app.priceDisport*2)*(12/3)*(app.qtySick1Y))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*25*(1-0.17)*app.qtySick1Y)
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick1Y*12*(1-0.17f))
    +
    (16538/5/((1)?1:4)*((0)?1:(app.qtySick1Y)))
    +
    ((177793+32287)*(((app.qtySick1Y))));
    
    // botox
    long long botox_total_ALL_3M =
//    ((((app.qtySick3M*(app.sickPercent1Group/100))+(app.qtySick3M*(app.sickPercent2Group/100))))*app.gdpPerHabitant/4)
//    +
    ((app.priceBotox*3)*(3/3)*(app.qtySick3M))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*app.qtySick3M*(1-0.127))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick3M*3*(1-0.127f))
    +
    (22250/5/(4)*((app.qtySick3M)))
    +
    ((177793+32287)*((0)?1:((app.qtySick3M))));
    
    long long botox_total_ALL_1Y =
//    ((((app.qtySick1Y*(app.sickPercent1Group/100))+(app.qtySick1Y*(app.sickPercent2Group/100)))*app.gdpPerHabitant*(1-0.127)))
//    +
    ((app.priceBotox*3)*(12/3)*(app.qtySick1Y))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*25*(1-0.127)*app.qtySick1Y)
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick1Y*12*(1-0.127f))
    +
    (22250/5/((1)?1:4)*((0)?1:(1?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((0)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    
    // xeomin
    long long xeomin_total_ALL_3M =
//    ((((app.qtySick3M*(app.sickPercent1Group/100))+(app.qtySick3M*(app.sickPercent2Group/100))))*app.gdpPerHabitant/4)
//    +
    ((app.priceXeomin*4)*(3/3)*(app.qtySick3M))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*app.qtySick3M*(1-0.1233))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick3M*3*(1-0.1233f))
    +
    (24029/5/(4)*((app.qtySick3M)))
    +
    ((177793+32287)*((0)?1:((0?app.qtySick1Y:app.qtySick3M))));
    
    long long xeomin_total_ALL_1Y =
//    ((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*(1-0.1233)))
//    +
    ((app.priceXeomin*4)*(12/3)*(app.qtySick1Y))
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*25*(1-0.1233)*app.qtySick1Y)
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick1Y*12*(1-0.1233f))
    +
    (24029/5/((1)?1:4)*((0)?1:(1?app.qtySick1Y:app.qtySick3M)))
    +
    ((177793+32287)*((0)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    // none
    long long none_total_ALL_3M =
//    (((((app.gdpPerHabitant*(app.sickPercent1Group/100))+(app.gdpPerHabitant*(app.sickPercent2Group/100)))*5*(1-0.0333)))/4)
//    +
    0
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*3*app.qtySick3M*(1-0.0333))
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick3M*3*(1-0.0333f))
    +
    0
    +
    ((177793+32287)*((0)?1:((0?app.qtySick1Y:app.qtySick3M))));
    
    long long none_total_ALL_1Y =
//    ((((app.qtySick1Y*(app.sickPercent1Group/100))+(app.qtySick1Y*(app.sickPercent2Group/100)))*app.gdpPerHabitant*(1-0.0333)))
//    +
    0
    +
    ((app.averageWage1+app.averageWage2)*0.5f*0.8f*25*(1-0.0333)*app.qtySick1Y)
    +
    (((app.invPayment1Group+app.monthlyPayment1Group)*app.sickPercent1Group/100
      +(app.invPayment2Group+app.monthlyPayment2Group)*app.sickPercent2Group/100
      +(app.invPayment3Group+app.monthlyPayment3Group)*app.sickPercent3Group/100)*app.qtySick1Y*12*(1-0.0333f))
    +
    0
    +
    ((177793+32287)*((0)?1:((1?app.qtySick1Y:app.qtySick3M))));
    
    // botox 3m
    long long botox_ALL_3M = (botox_total_ALL_3M-disport_total_ALL_3M);
    self.disportVsBotoxValue3M.text = MONEY_NUMBER(ABS(botox_ALL_3M));
    self.disportVsBotoxSign3M.text = (botox_ALL_3M>0)?@"Уменьшатся":@"Увеличатся";
    self.disportVsBotoxSign3M.textColor = (botox_ALL_3M>0)?GREEN_COLOR:RED_COLOR;

    // botox 1Y
    long long botox_ALL_1Y = (botox_total_ALL_1Y-disport_total_ALL_1Y);
    self.disportVsBotoxValue1Y.text = MONEY_NUMBER(ABS(botox_ALL_1Y));
    self.disportVsBotoxSign1Y.text = (botox_ALL_1Y>0)?@"Уменьшатся":@"Увеличатся";
    self.disportVsBotoxSign1Y.textColor = (botox_ALL_1Y>0)?GREEN_COLOR:RED_COLOR;

    // xeomin 3m
    long long xeomin_ALL_3M = (xeomin_total_ALL_3M-disport_total_ALL_3M);
    self.disportVsXeominValue3M.text = MONEY_NUMBER(ABS(xeomin_ALL_3M));
    self.disportVsXeominSign3M.text = (xeomin_ALL_3M>0)?@"Уменьшатся":@"Увеличатся";
    self.disportVsXeominSign3M.textColor = (xeomin_ALL_3M>0)?GREEN_COLOR:RED_COLOR;
    
    // xeomin 1Y
    long long xeomin_ALL_1Y = (xeomin_total_ALL_1Y-disport_total_ALL_1Y);
    self.disportVsXeominValue1Y.text = MONEY_NUMBER(ABS(xeomin_ALL_1Y));
    self.disportVsXeominSign1Y.text = (xeomin_ALL_1Y>0)?@"Уменьшатся":@"Увеличатся";
    self.disportVsXeominSign1Y.textColor = (xeomin_ALL_1Y>0)?GREEN_COLOR:RED_COLOR;

    // none 3m
    long long none_ALL_3M = (none_total_ALL_3M-disport_total_ALL_3M);
    self.disportVsNoneValue3M.text = MONEY_NUMBER(ABS(none_ALL_3M));
    self.disportVsNoneSign3M.text = (none_ALL_3M>0)?@"Уменьшатся":@"Увеличатся";
    self.disportVsNoneSign3M.textColor = (none_ALL_3M>0)?GREEN_COLOR:RED_COLOR;
    
    // none 1Y
    long long none_ALL_1Y = (none_total_ALL_1Y-disport_total_ALL_1Y);
    self.disportVsNoneValue1Y.text = MONEY_NUMBER(ABS(none_ALL_1Y));
    self.disportVsNoneSign1Y.text = (none_ALL_1Y>0)?@"Уменьшатся":@"Увеличатся";
    self.disportVsNoneSign1Y.textColor = (none_ALL_1Y>0)?GREEN_COLOR:RED_COLOR;
    
}

@end
