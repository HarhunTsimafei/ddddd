//
//  ChartHelper.m
//  ipsen
//
//  Created by oles on 7/16/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "ChartHelper.h"
#import <CorePlot-CocoaTouch.h>

@implementation ChartHelper

static NSNumberFormatter *currencyFormatter;
static NSNumberFormatter *numberFormatter;


+ (NSNumberFormatter *)currencyFormatter {
    if (!currencyFormatter) {
        NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
        //[currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        currencyFormatter.usesGroupingSeparator = YES;
        NSLocale *ruLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
        [currencyFormatter setLocale:ruLocale];
        [currencyFormatter setMaximumFractionDigits:0];
        [currencyFormatter setMinimumFractionDigits:0];
        [currencyFormatter setGroupingSeparator:@","];
        [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        //[numberFormatter setCurrencyCode:@"RUB"];
        //[numberFormatter setCurrencySymbol:@"₷"];
        return currencyFormatter;
    }
    return currencyFormatter;
}

+ (NSNumberFormatter *)numberFormatter {
    if (!numberFormatter) {
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        numberFormatter.usesGroupingSeparator = YES;
        [numberFormatter setMaximumFractionDigits:0];
        [numberFormatter setMinimumFractionDigits:0];
        [numberFormatter setGroupingSeparator:@" "];
        return numberFormatter;
    }
    return numberFormatter;
}


@end
