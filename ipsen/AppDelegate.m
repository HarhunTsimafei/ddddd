//
//  AppDelegate.m
//  ipsen
//
//  Created by oles on 7/15/14.
//  Copyright (c) 2014 undersky. All rights reserved.
//

#import "AppDelegate.h"
#import "Config.h"
#import <TMDiskCache.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [Fabric with:@[CrashlyticsKit]];

    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIColor colorWithRed:11.0f/255.0f green:63.0f/255.0f blue:83.0f/255.0f alpha:1.0f], NSForegroundColorAttributeName, [UIFont systemFontOfSize:17.0f], NSFontAttributeName, nil]];
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor colorWithRed:14.0f/255.0f green:122.0f/255.0f blue:171.0f/255.0f alpha:1.0f], NSForegroundColorAttributeName, [UIFont systemFontOfSize:17.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [self initData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataChanged) name:NOTIFICATION_DATA_CHANGED object:nil];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (NSDictionary *)dictionaryWithContentsOfData:(NSData *)data
{
	// uses toll-free bridging for data into CFDataRef and CFPropertyList into NSDictionary
	CFPropertyListRef plist =  CFPropertyListCreateFromXMLData(kCFAllocatorDefault, (__bridge CFDataRef)data,
															   kCFPropertyListImmutable,
															   NULL);
	// we check if it is the correct type and only return it if it is
	if ([(__bridge id)plist isKindOfClass:[NSDictionary class]])
	{
		return (__bridge NSDictionary *)plist;
	}
	else
	{
		// clean up ref
		CFRelease(plist);
		return nil;
	}
}

- (void)initData {
    
    // first time
    NSNumber *loaded = [[NSUserDefaults standardUserDefaults] objectForKey:DATA_LOADED_KEY];
    if (loaded == nil) {
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"];
        self.dict = [[NSDictionary alloc] initWithContentsOfFile:filePath];

        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:DATA_LOADED_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[TMDiskCache sharedCache] setObject:self.dict forKey:CACHE_KEY block:nil];
    } else {
        self.dict = (NSDictionary *)[[TMDiskCache sharedCache] objectForKey:CACHE_KEY];
    }
        
    // TODO: remove this after testing
    self.dict = (NSDictionary *)[[TMDiskCache sharedCache] objectForKey:CACHE_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_UI object:nil];
    
    NSURL *URL = [NSURL URLWithString:FILE_URL_PATH_DEF];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    
    NSURLSessionConfiguration *sessionConfig=[NSURLSessionConfiguration defaultSessionConfiguration];
    NSString *userPasswordString = [NSString stringWithFormat:@"%@:%@", @"admin", @"12345"];
    NSData * userPasswordData = [userPasswordString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64EncodedCredential = [userPasswordData base64EncodedStringWithOptions:0];
    NSString *authString = [NSString stringWithFormat:@"Basic %@", base64EncodedCredential];
    
    sessionConfig.HTTPAdditionalHeaders = @{@"Authorization": authString};
    //sessionConfig.requestCachePolicy = NSURLCacheStorageNotAllowed;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      NSDictionary *d = [self dictionaryWithContentsOfData:data];
                                      NSLog(@"%@", d);
                                      if (d != nil) {
                                          //self.dict = d;
                                          //[[TMDiskCache sharedCache] setObject:self.dict forKey:CACHE_KEY block:nil];
                                          //[[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_UI object:nil];
                                      }
                                  }];
    
    [task resume];
    
    //    NSDictionary *data = [NSDictionary dictionaryWithContentsOfURL:[NSURL URLWithString:FILE_URL_PATH_DEF]];
    //    if (data)
    //    {
    //        [[TMDiskCache sharedCache] setObject:data forKey:CACHE_KEY block:nil];
    //    }
    //    self.dict = (NSDictionary *)[[TMDiskCache sharedCache] objectForKey:CACHE_KEY];
    
    
    self.regions = [self.dict objectForKey:@"regions"];
    self.currentRegion = 0;
    
    
    NSArray* regions = self.regions;
    NSDictionary *region = regions[0];
    
    NSNumber *child = [region objectForKey:@"sick_child"];
    self.sickInAllRegions = [child longLongValue];
    
    NSNumber *disportPrice = [self.dict objectForKey:@"disport_price"];
    NSNumber *botoxPrice = [self.dict objectForKey:@"botox_price"];
    NSNumber *xeominPrice = [self.dict objectForKey:@"xeomin_price"];
    
    self.priceDisport  = disportPrice.floatValue;
    self.priceBotox    = botoxPrice.floatValue;
    self.priceXeomin   = xeominPrice.floatValue;
    
    self.gdpPerHabitant    = 520547;
    self.averageWage1      = 23369;
    self.averageWage2      = 26628;
    
    self.habitants         = 143666931;
    
    self.qtyInsult        = INSULT_KOEF*self.habitants;
    self.qtySick          = SICK_NOW*self.habitants;
    self.qtySick3M        = SICK_3M_APP;
    self.qtySick1Y        = SICK_1Y_APP;
    self.qtySick5Y        = SICK_5Y_APP;
    
    self.disportSickMonthes   = 5;
    self.botoxSickMonthes     = 5;
    self.xeominSickMonthes    = 5;
    self.noneSickMonthes      = 5;
    
    self.ashfortDisport     = 1.67f;
    self.ashfortBotox       = 1.17f;
    self.ashfortXeomin      = 0.87f;
    self.ashfortNone        = 0.67f;
    
    self.sickPercent1Group  = 12.7f;
    self.sickPercent2Group  = 32.7f;
    self.sickPercent3Group  = 34.6f;
    self.sickPercentNoneGroup  = 20.0f;
    
    self.invPayment1Group         = 7220;
    self.invPayment2Group         = 3610;
    self.invPayment3Group         = 1805;
    self.invPaymentNoneGroup      = 0;
    
    self.monthlyPayment1Group     = 2832;
    self.monthlyPayment2Group     = 2022;
    self.monthlyPayment3Group     = 1619;
    self.monthlyPaymentNoneGroup  = 0;
    
    self.monthsForCalc = 12;
    self.populationForCalc = 1;

    // Init data for analises costs
    
    self.mh = false;
    self.dbmh = 169777;
    self.nonemh = 170204;
    
    self.countCoursesPerYear = 3;
    
    self.costsOperations = (0.95*164112)+(440739*0.05);

    self.disportFourMouthExpenseEffect  = 100.f;
    self.disportOneYearExpenseEffect    = 97.5f;
    self.disportTwoYearsExpenseEffect   = 93.f;
    
    self.botoxFourMouthExpenseEffect    = 100.f;
    self.botoxOneYearExpenseEffect      = 93.f;
    self.botoxTwoYearsExpenseEffect     = 90.f;
    
    self.noneFourMouthExpenseEffect     = 100.f;
    self.noneOneYearExpenseEffect       = 56.f;
    self.noneTwoYearsExpenseEffect      = 48.f;
    
    self.disportCostsToxinA             = self.priceDisport*self.countCoursesPerYear;
    self.disportCostsMedicalHelp        = self.dbmh;
    self.disportCostsRelaxMedicalHelp   = 39900;
    self.disportCostsPension            = 154008;
    self.disportFourMouthCostsOperations            = lroundf(self.costsOperations*(1-(self.disportFourMouthExpenseEffect*0.01)));
    self.disportOneYearCostsOperations              = lroundf(self.costsOperations*(1-(self.disportOneYearExpenseEffect*0.01)));
    self.disportTwoYearsCostsOperations             = lroundf(self.costsOperations*(1-(self.disportTwoYearsExpenseEffect*0.01))/2);
    self.disportCostsPlastering         = 51274;
    self.disportCostsCompensation       = 9396;
    self.disportCostsAllowanceForKids   = 66000;
    
    self.botoxCostsToxinA               = self.priceBotox*self.countCoursesPerYear*2;
    self.botoxCostsMedicalHelp          = self.dbmh;
    self.botoxCostsRelaxMedicalHelp     = 39900;
    self.botoxCostsPension              = 154008;
    self.botoxFourMouthCostsOperations              = lroundf(self.costsOperations*(1-(self.botoxFourMouthExpenseEffect*0.01)));
    self.botoxOneYearCostsOperations                = lroundf(self.costsOperations*(1-(self.botoxOneYearExpenseEffect*0.01)));
    self.botoxTwoYearsCostsOperations               = lroundf(self.costsOperations*(1-(self.botoxTwoYearsExpenseEffect*0.01))/2);
    self.botoxCostsPlastering           = 51274;
    self.botoxCostsCompensation         = 18256;
    self.botoxCostsAllowanceForKids     = 66000;
    
    self.noneCostsToxinA                = 0;
    self.noneCostsMedicalHelp           = self.nonemh;
    self.noneCostsRelaxMedicalHelp      = 39900;
    self.noneCostsPension               = 154008;
    self.noneFourMouthCostsOperations               = lroundf(self.costsOperations*(1-(self.noneFourMouthExpenseEffect*0.01)));
    self.noneOneYearCostsOperations                 = lroundf(self.costsOperations*(1-(self.noneOneYearExpenseEffect*0.01)));
    self.noneTwoYearsCostsOperations                = lroundf(self.costsOperations*(1-(self.noneTwoYearsExpenseEffect*0.01))/2);
    self.noneCostsPlastering            = 51274;
    self.noneCostsCompensation          = 77687;
    self.noneCostsAllowanceForKids      = 66000;
    
    self.populationForCalcApp = 1;
    self.yearForCalc = 1;
}

- (void) recountData {
    self.disportCostsToxinA             = self.priceDisport*self.countCoursesPerYear;
    self.botoxCostsToxinA               = self.priceBotox*self.countCoursesPerYear*2;
    
    if(self.mh) {
        self.disportCostsMedicalHelp        = self.writemh;
        self.botoxCostsMedicalHelp          = self.writemh;
        self.noneCostsMedicalHelp           = self.writemh;
    } else {
        self.disportCostsMedicalHelp        = self.dbmh;
        self.botoxCostsMedicalHelp          = self.dbmh;
        self.noneCostsMedicalHelp           = self.nonemh;
    }

    
}

- (void)calcExpensesAnysis {
    
    // 1. gdp
    if (self.monthsForCalc == 12) {
        if (self.populationForCalc == 1) {
            self.disportGdpLossTotal = ((((self.gdpPerHabitant*(self.sickPercent1Group/100))+(self.gdpPerHabitant*(self.sickPercent2Group/100)))*(1-0.17)));
            self.botoxGdpLossTotal = ((((self.gdpPerHabitant*(self.sickPercent1Group/100))+(self.gdpPerHabitant*(self.sickPercent2Group/100)))*(1-0.127)));
            self.xeominGdpLossTotal = ((((self.gdpPerHabitant*(self.sickPercent1Group/100))+(self.gdpPerHabitant*(self.sickPercent2Group/100)))*(1-0.1233)));
            self.noneGdpLossTotal = ((((self.gdpPerHabitant*(self.sickPercent1Group/100))+(self.gdpPerHabitant*(self.sickPercent2Group/100)))*(1-0.0333)));
        } else {
            //self.disportGdpLossTotal = ((((self.gdpPerHabitant*(self.sickPercent1Group/100))+(self.gdpPerHabitant*(self.sickPercent2Group/100)))*(1-0.17)));
            self.disportGdpLossTotal = ((((self.qtySick1Y*(self.sickPercent1Group/100))+(self.qtySick1Y*(self.sickPercent2Group/100)))*self.gdpPerHabitant*(1-0.17)));
            self.botoxGdpLossTotal = ((((self.qtySick1Y*(self.sickPercent1Group/100))+(self.qtySick1Y*(self.sickPercent2Group/100)))*self.gdpPerHabitant*(1-0.127)));
            self.xeominGdpLossTotal = ((((self.qtySick1Y*(self.sickPercent1Group/100))+(self.qtySick1Y*(self.sickPercent2Group/100)))*self.gdpPerHabitant*(1-0.1233)));
            self.noneGdpLossTotal = ((((self.qtySick1Y*(self.sickPercent1Group/100))+(self.qtySick1Y*(self.sickPercent2Group/100)))*self.gdpPerHabitant*(1-0.0333)));
        }
    } else {
        if (self.populationForCalc == 1) {
            self.disportGdpLossTotal = ((((self.gdpPerHabitant*(self.sickPercent1Group/100))+(self.gdpPerHabitant*(self.sickPercent2Group/100)))*5*(1-0.17)))/4;
            self.botoxGdpLossTotal = ((((self.gdpPerHabitant*(self.sickPercent1Group/100))+(self.gdpPerHabitant*(self.sickPercent2Group/100)))*5*(1-0.127)))/4;
            self.xeominGdpLossTotal = ((((self.gdpPerHabitant*(self.sickPercent1Group/100))+(self.gdpPerHabitant*(self.sickPercent2Group/100)))*5*(1-0.1233)))/4;
            self.noneGdpLossTotal = ((((self.gdpPerHabitant*(self.sickPercent1Group/100))+(self.gdpPerHabitant*(self.sickPercent2Group/100)))*5*(1-0.0333)))/4;
        } else {
            self.disportGdpLossTotal = (((self.qtySick3M*(self.sickPercent1Group/100))+(self.qtySick3M*(self.sickPercent2Group/100))))*self.gdpPerHabitant/4;
            self.botoxGdpLossTotal = (((self.qtySick3M*(self.sickPercent1Group/100))+(self.qtySick3M*(self.sickPercent2Group/100))))*self.gdpPerHabitant/4;
            self.xeominGdpLossTotal = (((self.qtySick3M*(self.sickPercent1Group/100))+(self.qtySick3M*(self.sickPercent2Group/100))))*self.gdpPerHabitant/4;
            self.noneGdpLossTotal = (((self.qtySick3M*(self.sickPercent1Group/100))+(self.qtySick3M*(self.sickPercent2Group/100))))*self.gdpPerHabitant/4;
        }
    }
    
    // 2. price of drug per month
    self.disportTotalPrice = (self.priceDisport*2)*(self.monthsForCalc/3)*((self.populationForCalc==1)?1:((self.monthsForCalc==12)?self.qtySick1Y:(self.qtySick3M)));
    self.botoxTotalPrice = (self.priceBotox*3)*(self.monthsForCalc/3)*((self.populationForCalc==1)?1:((self.monthsForCalc==12)?self.qtySick1Y:(self.qtySick3M)));
    self.xeominTotalPrice = (self.priceXeomin*4)*(self.monthsForCalc/3)*((self.populationForCalc==1)?1:((self.monthsForCalc==12)?self.qtySick1Y:(self.qtySick3M)));
    self.noneTotalPrice = 0;
    
    // 3. payouts (disablement) per month
    if (self.monthsForCalc == 12) {
        if (self.populationForCalc == 1) {
            self.disportDisPayouts = (self.averageWage1+self.averageWage2)*0.5f*0.8f*5*(1-0.17);
            self.botoxDisPayouts = (self.averageWage1+self.averageWage2)*0.5f*0.8f*5*(1-0.127);
            self.xeominDisPayouts = (self.averageWage1+self.averageWage2)*0.5f*0.8f*5*(1-0.1233);
            self.noneDisPayouts = (self.averageWage1+self.averageWage2)*0.5f*0.8f*5*(1-0.0333);
        } else {
            self.disportDisPayouts = (self.averageWage1+self.averageWage2)*0.5f*0.8f*25*(1-0.17)*self.qtySick1Y;
            self.botoxDisPayouts = (self.averageWage1+self.averageWage2)*0.5f*0.8f*25*(1-0.127)*self.qtySick1Y;
            self.xeominDisPayouts = (self.averageWage1+self.averageWage2)*0.5f*0.8f*25*(1-0.1233)*self.qtySick1Y;
            self.noneDisPayouts = (self.averageWage1+self.averageWage2)*0.5f*0.8f*25*(1-0.0333)*self.qtySick1Y;
        }
    } else {
        if (self.populationForCalc == 1) {
            self.disportDisPayouts = (self.averageWage1+self.averageWage2)*0.5f*0.8f*3*(1-0.17);
            self.botoxDisPayouts = (self.averageWage1+self.averageWage2)*0.5f*0.8f*3*(1-0.127);
            self.xeominDisPayouts = (self.averageWage1+self.averageWage2)*0.5f*0.8f*3*(1-0.1233);
            self.noneDisPayouts = (self.averageWage1+self.averageWage2)*0.5f*0.8f*3*(1-0.0333);
        } else {
            self.disportDisPayouts = (self.averageWage1+self.averageWage2)*0.5f*0.8f*3*self.qtySick3M*(1-0.17);
            self.botoxDisPayouts = (self.averageWage1+self.averageWage2)*0.5f*0.8f*3*self.qtySick3M*(1-0.127);
            self.xeominDisPayouts = (self.averageWage1+self.averageWage2)*0.5f*0.8f*3*self.qtySick3M*(1-0.1233);
            self.noneDisPayouts = (self.averageWage1+self.averageWage2)*0.5f*0.8f*3*self.qtySick3M*(1-0.0333);
        }
    }
    
    // 4. payouts (inv) per month
    if (self.monthsForCalc == 12) {
        if (self.populationForCalc == 1) {
            self.disportInvPayouts = ((self.invPayment1Group+self.monthlyPayment1Group)*self.sickPercent1Group/100
                                      +(self.invPayment2Group+self.monthlyPayment2Group)*self.sickPercent2Group/100
                                      +(self.invPayment3Group+self.monthlyPayment3Group)*self.sickPercent3Group/100)*1*12*(1-0.17f);
            self.botoxInvPayouts = ((self.invPayment1Group+self.monthlyPayment1Group)*self.sickPercent1Group/100
                                    +(self.invPayment2Group+self.monthlyPayment2Group)*self.sickPercent2Group/100
                                    +(self.invPayment3Group+self.monthlyPayment3Group)*self.sickPercent3Group/100)*1*12*(1-0.127f);
            self.xeominInvPayouts = ((self.invPayment1Group+self.monthlyPayment1Group)*self.sickPercent1Group/100
                                     +(self.invPayment2Group+self.monthlyPayment2Group)*self.sickPercent2Group/100
                                     +(self.invPayment3Group+self.monthlyPayment3Group)*self.sickPercent3Group/100)*1*12*(1-0.1233f);
            self.noneInvPayouts = ((self.invPayment1Group+self.monthlyPayment1Group)*self.sickPercent1Group/100
                                   +(self.invPayment2Group+self.monthlyPayment2Group)*self.sickPercent2Group/100
                                   +(self.invPayment3Group+self.monthlyPayment3Group)*self.sickPercent3Group/100)*1*12*(1-0.0333f);
        } else {
            self.disportInvPayouts = ((self.invPayment1Group+self.monthlyPayment1Group)*self.sickPercent1Group/100
                                      +(self.invPayment2Group+self.monthlyPayment2Group)*self.sickPercent2Group/100
                                      +(self.invPayment3Group+self.monthlyPayment3Group)*self.sickPercent3Group/100)*self.qtySick1Y*12*(1-0.17f);
            self.botoxInvPayouts = ((self.invPayment1Group+self.monthlyPayment1Group)*self.sickPercent1Group/100
                                    +(self.invPayment2Group+self.monthlyPayment2Group)*self.sickPercent2Group/100
                                    +(self.invPayment3Group+self.monthlyPayment3Group)*self.sickPercent3Group/100)*self.qtySick1Y*12*(1-0.127f);
            self.xeominInvPayouts = ((self.invPayment1Group+self.monthlyPayment1Group)*self.sickPercent1Group/100
                                     +(self.invPayment2Group+self.monthlyPayment2Group)*self.sickPercent2Group/100
                                     +(self.invPayment3Group+self.monthlyPayment3Group)*self.sickPercent3Group/100)*self.qtySick1Y*12*(1-0.1233f);
            self.noneInvPayouts = ((self.invPayment1Group+self.monthlyPayment1Group)*self.sickPercent1Group/100
                                   +(self.invPayment2Group+self.monthlyPayment2Group)*self.sickPercent2Group/100
                                   +(self.invPayment3Group+self.monthlyPayment3Group)*self.sickPercent3Group/100)*self.qtySick1Y*12*(1-0.0333f);
        }
    } else {
        if (self.populationForCalc == 1) {
            self.disportInvPayouts = ((self.invPayment1Group+self.monthlyPayment1Group)*self.sickPercent1Group/100
                                      +(self.invPayment2Group+self.monthlyPayment2Group)*self.sickPercent2Group/100
                                      +(self.invPayment3Group+self.monthlyPayment3Group)*self.sickPercent3Group/100)*1*3*(1-0.17f);
            self.botoxInvPayouts = ((self.invPayment1Group+self.monthlyPayment1Group)*self.sickPercent1Group/100
                                    +(self.invPayment2Group+self.monthlyPayment2Group)*self.sickPercent2Group/100
                                    +(self.invPayment3Group+self.monthlyPayment3Group)*self.sickPercent3Group/100)*1*3*(1-0.127f);
            self.xeominInvPayouts = ((self.invPayment1Group+self.monthlyPayment1Group)*self.sickPercent1Group/100
                                     +(self.invPayment2Group+self.monthlyPayment2Group)*self.sickPercent2Group/100
                                     +(self.invPayment3Group+self.monthlyPayment3Group)*self.sickPercent3Group/100)*1*3*(1-0.1233f);
            self.noneInvPayouts = ((self.invPayment1Group+self.monthlyPayment1Group)*self.sickPercent1Group/100
                                   +(self.invPayment2Group+self.monthlyPayment2Group)*self.sickPercent2Group/100
                                   +(self.invPayment3Group+self.monthlyPayment3Group)*self.sickPercent3Group/100)*1*3*(1-0.0333f);
        } else {
            self.disportInvPayouts = ((self.invPayment1Group+self.monthlyPayment1Group)*self.sickPercent1Group/100
                                      +(self.invPayment2Group+self.monthlyPayment2Group)*self.sickPercent2Group/100
                                      +(self.invPayment3Group+self.monthlyPayment3Group)*self.sickPercent3Group/100)*self.qtySick3M*3*(1-0.17f);
            self.botoxInvPayouts = ((self.invPayment1Group+self.monthlyPayment1Group)*self.sickPercent1Group/100
                                    +(self.invPayment2Group+self.monthlyPayment2Group)*self.sickPercent2Group/100
                                    +(self.invPayment3Group+self.monthlyPayment3Group)*self.sickPercent3Group/100)*self.qtySick3M*3*(1-0.127f);
            self.xeominInvPayouts = ((self.invPayment1Group+self.monthlyPayment1Group)*self.sickPercent1Group/100
                                     +(self.invPayment2Group+self.monthlyPayment2Group)*self.sickPercent2Group/100
                                     +(self.invPayment3Group+self.monthlyPayment3Group)*self.sickPercent3Group/100)*self.qtySick3M*3*(1-0.1233f);
            self.noneInvPayouts = ((self.invPayment1Group+self.monthlyPayment1Group)*self.sickPercent1Group/100
                                   +(self.invPayment2Group+self.monthlyPayment2Group)*self.sickPercent2Group/100
                                   +(self.invPayment3Group+self.monthlyPayment3Group)*self.sickPercent3Group/100)*self.qtySick3M*3*(1-0.0333f);
        }
    }
    
    // 5. side treatment per month
    self.disportSideTreatment = 16538/5/((self.monthsForCalc==12)?1:4)*((self.populationForCalc==1)?1:(self.monthsForCalc==12?self.qtySick1Y:self.qtySick3M));
    self.botoxSideTreatment = 22250/5/((self.monthsForCalc==12)?1:4)*((self.populationForCalc==1)?1:(self.monthsForCalc==12?self.qtySick1Y:self.qtySick3M));
    self.xeominSideTreatment = 24029/5/((self.monthsForCalc==12)?1:4)*((self.populationForCalc==1)?1:(self.monthsForCalc==12?self.qtySick1Y:self.qtySick3M));
    self.noneSideTreatment = 0;
    
    // 6. insult treatment per month
    self.disportInsultTreatment = (177793+32287)*((self.populationForCalc==1)?1:((self.monthsForCalc==12?self.qtySick1Y:self.qtySick3M)));
    self.botoxInsultTreatment = (177793+32287)*((self.populationForCalc==1)?1:((self.monthsForCalc==12?self.qtySick1Y:self.qtySick3M)));
    self.xeominInsultTreatment = (177793+32287)*((self.populationForCalc==1)?1:((self.monthsForCalc==12?self.qtySick1Y:self.qtySick3M)));
    self.noneInsultTreatment = (177793+32287)*((self.populationForCalc==1)?1:((self.monthsForCalc==12?self.qtySick1Y:self.qtySick3M)));
    
}

- (void)dataChanged {
    
    self.multiplier = self.populationForCalc*self.monthsForCalc;
    
    [self calcExpensesAnysis];
    // ... other calculations
    
    // ... and force to update UI
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_UI object:nil];
}

@end
