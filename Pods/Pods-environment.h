
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// CorePlot
#define COCOAPODS_POD_AVAILABLE_CorePlot
#define COCOAPODS_VERSION_MAJOR_CorePlot 1
#define COCOAPODS_VERSION_MINOR_CorePlot 5
#define COCOAPODS_VERSION_PATCH_CorePlot 1

// JBChartView
#define COCOAPODS_POD_AVAILABLE_JBChartView
#define COCOAPODS_VERSION_MAJOR_JBChartView 2
#define COCOAPODS_VERSION_MINOR_JBChartView 8
#define COCOAPODS_VERSION_PATCH_JBChartView 1

// TMCache
#define COCOAPODS_POD_AVAILABLE_TMCache
#define COCOAPODS_VERSION_MAJOR_TMCache 1
#define COCOAPODS_VERSION_MINOR_TMCache 2
#define COCOAPODS_VERSION_PATCH_TMCache 1

